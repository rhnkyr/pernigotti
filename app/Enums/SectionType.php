<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class SectionType extends Enum
{
    const Tight = 1;
    const Wide = 2;
    const Footer = 3;

    /**
     * Get the description for an enum value
     *
     * @param  int $value
     * @return string
     */
    public static function getDescription(int $value): string
    {
        switch ($value) {
            case self::Tight:
                return 'Dar';
                break;
            case self::Wide:
                return 'Geniş';
                break;
            case self::Footer:
                return 'Son Alan';
                break;
            default:
                return self::getKey($value);
        }
    }
}
