<div class="menu-top">
    <ul>

        @foreach($top_menu_elements as $tm)
            <li>
                <a href="{{route('page', ['slug' => $tm->page_route])}}">{{ $tm->name }}</a>
            </li>
        @endforeach
    </ul>
</div>