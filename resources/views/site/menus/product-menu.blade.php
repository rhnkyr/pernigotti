<div class="menu">
    <div class="menu-mobile">
        <a href="{{route('home')}}">
            <img class="img-fluid logo" src="{{asset('assets/images/logos/pernigotti.svg')}}" alt="Pernigotti"/>
        </a>
        <div class="hamburger-menu">
            <div class="bar"></div>
        </div>
    </div>

    <ul>
        <li class="menu-logo-holder">
            <a href="{{route('home')}}">
                <img class="img-fluid logo" src="{{asset('assets/images/logos/pernigotti.svg')}}" alt="Pernigotti"/>
            </a>
        </li>

        @foreach($product_menu_elements as $category)
            <li>
                <a class="menu-title" href="{{route('products', ['slug' => $category->slug])}}">{{$category->name}} <i
                            class="ion-ios-arrow-down"></i></a>
                <ul>
                    @foreach($category->sub_categories as $sub_category)
                        <li>
                            <a href="{{route('products', ['slug' => $category->slug])}}#{{$sub_category->slug}}">{{$sub_category->name}}</a>
                            <ul>
                                @foreach($sub_category->ordered_products as $product)
                                    <li>
                                        <a href="{{route('product',['category'=> $category->slug, 'slug'=>$product->slug])}}">
                                            {{$product->name}} {{$product->weight}}g <i
                                                    class="ion-android-arrow-forward"></i>
                                        </a>
                                        <img class="img-fluid" src="{{$product->image}}"
                                             alt="{{$sub_category->name}} {{$product->name}} {{$product->weight}}g">
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    @endforeach
                </ul>
            </li>
        @endforeach

        @foreach($top_menu_elements as $tm)
            <li class="menu-dup">
                <a href="{{route('page', ['slug' => $tm->page_route])}}">{{ $tm->name }}</a>
            </li>
        @endforeach

    </ul>
</div>