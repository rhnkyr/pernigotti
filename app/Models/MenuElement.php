<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 12 Apr 2018 14:59:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MenuElement
 *
 * @property int $id
 * @property string $menu_section
 * @property string $name
 * @property string $page_route
 * @property int $order
 * @property int $active
 *
 * @package App\Models
 */
class MenuElement extends Eloquent
{
    public $timestamps = false;

    protected $casts = [
        'order'  => 'int',
        'active' => 'int'
    ];

    protected $fillable = [
        'menu_section',
        'name',
        'page_route',
        'order',
        'active'
    ];

    public function parent()
    {
        return $this->hasOne(Menu::class, 'section', 'menu_section');
    }
}
