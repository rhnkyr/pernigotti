@extends('site.layouts.site')

@section('content')

    @include('site.parts.hero', ['class'=>'product-head-image','parent' => $page])

    <!-- Section Activity  -->
    <section>
        <div class="container m-bottom-100 m-top-70">
            <div class="row">
                <div class="col-md-12 color-brown m-bottom-30 sss">
                    <h2>Sıkça Sorulan Sorular</h2>

                    @foreach($faqs as $faq)

                    <h3>{{$loop->index + 1}}. {{$faq->title}}</h3>
                    <p>{{$faq->content}}</p>

                    @endforeach

                </div>
            </div>
        </div>
    </section>

@endsection