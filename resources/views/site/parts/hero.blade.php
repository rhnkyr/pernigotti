<div class="{{$class}} t-150" style="background: url('{{$parent->image}}') no-repeat;background-size: cover">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">{{$site_settings->site_title}}</a></li>
                    <li class="breadcrumb-image">
                        <img class="img-fluid" src="{{asset('assets/images/icons/right.svg')}}" alt=""/>
                    </li>
                    @if(isset($parent,$child))
                        <li class="breadcrumb-item"><a href="/urunler/{{$parent->route}}">{{$parent->name}}</a></li>
                        <li class="breadcrumb-image">
                            <img class="img-fluid" src="{{asset('assets/images/icons/right.svg')}}" alt=""/>
                        </li>
                        @if(!isset($child->weight))
                            <li class="breadcrumb-item active">{{$child->name}}</li>
                        @else
                            <li class="breadcrumb-item active">{{ucfirst(mb_strtolower($child->category->name))}} {{$child->name}} {{$child->weight}}g</li>
                        @endif
                    @else
                        <li class="breadcrumb-item active">{{$parent->name}}</li>
                    @endif
                </ol>
            </div>
            @if(!isset($child))
                @if($sub_class = !isset($categories) ? 'm-top-70' : '') @endif
                <div class="ml-auto col-lg-7 mr-auto head-style text-center {{$sub_class}}">
                    <h2>{{$page->top_text}}</h2>
                    <h1>{{$page->middle_text}}</h1>
                    <p>{!!  $page->description !!}</p>
                </div>

                @if(isset($categories))
                    <div class="ml-auto col-9 mr-auto">
                        <div class="row">
                            @if ($count = count($categories)) @endif
                            @foreach($categories as $category)
                                <div class="ml-auto @if($count > 2 )col-4 @else col-5 @endif tabs">
                                    <a href="#{{$category->slug}}">
                                        <h4>{{$category->name}}</h4>
                                        <img class="img-fluid" src="{{$category->image}}" alt="{{$category->name}}"/>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif

                <div class="ml-auto col-md-12 mr-auto scrolldown text-center">
                    <a href="#start">
                        <img class="img-fluid" src="{{asset('assets/images/icons/scrolldown.svg')}}" alt=""/>
                    </a>
                </div>
            @endif
        </div>
    </div>
</div>