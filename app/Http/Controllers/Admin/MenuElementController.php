<?php

namespace App\Http\Controllers\Admin;

use App\Models\Menu;
use App\Models\MenuElement;
use Illuminate\Http\Request;

class MenuElementController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($menu)
    {
        $menu_elements = MenuElement::with('parent')->where('menu_section', $menu)->orderBy('order', 'asc')->get();
        return view('admin.content.menu-element.list', compact('menu_elements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($menu)
    {
        $menu = Menu::where('section', $menu)->first();

        return view('admin.content.menu-element.create', compact('menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $menu)
    {
        $this->validate($request, [
            'name'       => 'required|max:255',
            'page_route' => 'required|max:255',
        ]);


        $order = MenuElement::where('menu_section', $menu)->max('order');

        $element = new MenuElement();

        $element->menu_section = $menu;
        $element->name         = $request->name;
        $element->page_route   = $request->page_route;
        $element->active       = $request->active;
        $element->order        = $order + 1;

        $element->save();

        return redirect()->route('menu.elements.index', ['menu' => $menu]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $menu
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($menu, $id)
    {
        $element = MenuElement::find($id);

        if ($element === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return redirect()->route('menu.elements.list', ['menu' => $menu]);
        }

        $menu = Menu::where('section', $menu)->first();

        return view('admin.content.menu-element.edit', compact('element', 'menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $menu, $id)
    {
        $this->validate($request, [
            'name'       => 'required|max:255',
            'page_route' => 'required|max:255',
        ]);

        $element = MenuElement::find($id);

        $element->menu_section = $menu;
        $element->name         = $request->name;
        $element->page_route   = $request->page_route;
        $element->active       = $request->active;

        $element->save();

        return redirect()->route('menu.elements.index', ['menu' => $menu]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($menu,$id)
    {
        $element = MenuElement::find($id);

        if ($element === null) {
            return ['status' => false];
        }

        $element->delete();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return ['status' => true];
    }

    public function sort()
    {
        $i = 1;

        foreach ($_POST['item'] as $value) {

            $faq        = MenuElement::find($value);
            $faq->order = $i;
            $faq->save();

            $i++;
        }
        return ['status' => true];

    }
}
