//== Class definition
var FormRepeater = function() {

    //== Private functions
    var r1 = function() {
        $('#m_repeater_1').repeater({            
            initEmpty: false,

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {

                swal({
                    title: "Uyarı!",
                    text: "Silmek istediğinize emin misiniz?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: 'btn btn-danger',
                    confirmButtonText: "Evet",
                    cancelButtonText: "İptal"
                }).then(function (result) {
                    if (result.value) {
                        $(this).slideUp(deleteElement);
                    }
                });
            }   
        });
    }

    var r2 = function() {
        $('#m_repeater_2').repeater({            
            initEmpty: false,

            show: function() {
                $(this).slideDown();                               
            },

            hide: function(deleteElement) {
                swal({
                    title: "Uyarı!",
                    text: "Silmek istediğinize emin misiniz?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: 'btn btn-danger',
                    confirmButtonText: "Evet",
                    cancelButtonText: "İptal"
                }).then(function (result) {
                    if (result.value) {
                        $(this).slideUp(deleteElement);
                    }
                });
            }      
        });
    }


    var r3 = function() {
        $('#m_repeater_3').repeater({            
            initEmpty: false,

            show: function() {
                $(this).slideDown();                               
            },

            hide: function(deleteElement) {
                swal({
                    title: "Uyarı!",
                    text: "Silmek istediğinize emin misiniz?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: 'btn btn-danger',
                    confirmButtonText: "Evet",
                    cancelButtonText: "İptal"
                }).then(function (result) {
                    if (result.value) {
                        $(this).slideUp(deleteElement);
                    }
                });
            }      
        });
    }
    return {
        // public functions
        init: function() {
            r1();
            r2();
            r3();
        }
    };
}();

jQuery(document).ready(function() {
    FormRepeater.init();
});

    