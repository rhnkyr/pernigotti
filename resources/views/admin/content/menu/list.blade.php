@extends('admin.layouts.main')
@section('content')
    @include('admin.parts.breadcrumb',['page'=> 'Menüler'])
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Liste
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-lg-12 m--align-right">
                            <a href="{{route('menus.create')}}"
                               class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-plus-circle"></i>
													<span>
														Yeni Menü
													</span>
												</span>
                            </a>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet">
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="m-section__content">
                                <table class="table table-striped m-table">
                                    <thead>
                                    <tr>
                                        <th>
                                            Menü Adı
                                        </th>
                                        <th>
                                            Menü Yeri
                                        </th>
                                        <th>
                                            İşlemler
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($menus as $menu)
                                        <tr>
                                            <td style="width: 70%">
                                                {{$menu->name}}
                                            </td>
                                            <td style="width: 15%">
                                                {{$menu->section}}
                                            </td>
                                            <td style="width: 15%">
                                                <a href="{{route('menus.edit',['id'=>$menu->id])}}"
                                                   class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                                   title="Düzenle">
                                                    <i class="la la-edit"></i>
                                                </a>
                                                @if($menu->deletable === 1)
                                                    <a href="#" data-id="{{$menu->id}}"
                                                       class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill del"
                                                       title="Sil">
                                                        <i class="la la-trash"></i>
                                                    </a>
                                                @endif
                                                @if($menu->elements)
                                                    <a href="{{route('menu.elements.index',['menu' => $menu->section])}}"
                                                       class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill"
                                                       title="Menü Alt Başlıklar">
                                                        <i class="la la-plus-circle"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--end::Section-->
                    </div>
                    <!--end::Form-->
                </div>

            </div>
        </div>
    </div>
@endsection

@section('page_js')

    <script>

        $(function () {
            $('body').on('click', 'a.del', function (e) {
                e.preventDefault();
                var id = $(this).data('id');

                swal({
                    title: "Uyarı!",
                    text: "Silmek istediğinize emin misiniz?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: 'btn btn-danger',
                    confirmButtonText: "Evet",
                    cancelButtonText: "İptal"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: '{{route('menus.index')}}/' + id,
                            dataType: "json",
                            type: 'DELETE',
                            success: function (result) {
                                if (result.status)
                                    location.reload();
                            }
                        });
                    }
                });
            });
        });
    </script>
@endsection

