<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 19 Apr 2018 17:36:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SiteSetting
 * 
 * @property int $id
 * @property string $site_title
 * @property string $facebook
 * @property string $youtube
 * @property string $instagram
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $privacy
 * @property string $legal
 *
 * @package App\Models
 */
class SiteSetting extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'site_title',
		'facebook',
		'youtube',
		'instagram',
		'meta_description',
		'meta_keywords',
		'privacy',
		'legal'
	];
}
