@extends('admin.layouts.main')
@section('content')
    <!-- BEGIN: Subheader -->
    @include('admin.parts.breadcrumb',['page'=> 'Slides'])
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Slide Bilgiler
                                </h3>
                            </div>
                        </div>
                    </div>
                    {!! Form::open(['route' => 'slides.store', 'class'=>'m-form', 'files' => true]) !!}
                    <div class="m-portlet__body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form-group m-form__group col-lg-9">
                            {!! Form::label('image', 'Görsel :'); !!}
                            {!! Form::file('image'); !!}
                        </div>

                        <div class="form-group m-form__group col-lg-9">
                            {!! Form::label('route', 'Hedef Route (Rota - Slug) :'); !!}
                            {!! Form::text('route', null, ['class' => 'form-control m-input']); !!}
                        </div>

                        <div class="form-group m-form__group col-lg-9">
                            {!! Form::label('top_text', 'Header Üst Yazı :'); !!}
                            {!! Form::textarea('top_text',null, ['class' => 'form-control m-input']); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-9">
                            {!! Form::label('middle_text', 'Header Orta Yazı :'); !!}
                            {!! Form::textarea('middle_text',null, ['class' => 'form-control m-input']); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-9">
                            {!! Form::label('description', 'Header Alt Yazı :'); !!}
                            {!! Form::textarea('description',null, ['class' => 'form-control m-input']); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-3">
                            {!! Form::label('active', 'Durum :'); !!}
                            {!! Form::select('active', ['0' => 'Pasif', '1' => 'Aktif'],null, ['class' => 'form-control m-input']); !!}
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <button class="btn btn-primary">
                                {!! Form::submit('Kaydet', ['class'=>'btn btn-primary']) !!}
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
@endsection
@section('page_js')
    <!--begin::Page Resources -->
    <script src="{{asset('assets/admin/assets/demo/default/custom/components/forms/widgets/summernote.js')}}"
            type="text/javascript"></script>
    <!--end::Page Resources -->
@endsection
