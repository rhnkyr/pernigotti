<style>
    .insta-slide a::before{
        content: "";
        position: absolute;
        background-color: rgba(65,32,31,0.40);
        width: 100%;
        height: 100%;
        z-index: 2;
    }

    .insta-slide .insta-h a img{
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        position: absolute;
        width: 100%;
    }

    .insta-slide .insta-v a img{
        left: 50%;
        -webkit-transform: translatex(-50%);
        -ms-transform: translatex(-50%);
        transform: translatex(-50%);
        position: absolute;
        height: 100%;
    }

    .insta-slide .col-md-2, .insta-slide .col-4{
        background-color: #262626;
    }
</style>
<section>
    <div class="container-fluid insta-slide">
        <div class="row m-0">
            @foreach($instagram_images as $image)
                @if($h = $image->images->standard_resolution->width > $image->images->standard_resolution->height ? 'insta-h' : '')@endif
                @if($v = $image->images->standard_resolution->width < $image->images->standard_resolution->height ? 'insta-v' : '')@endif
            <div class="col-md-2 p-0 {{$h}} {{$v}}">
                <a href="{{$image->link}}" target="_blank">
                    <img src="{{$image->images->standard_resolution->url}}" class="img-fluid">
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>