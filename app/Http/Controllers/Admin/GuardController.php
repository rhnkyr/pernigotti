<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;

class GuardController extends Controller
{

    public function index()
    {
        return view('admin.login');

    }

    public function check()
    {
        $inputs = Input::all();
        if (\Auth::attempt(['username' => $inputs['username'], 'password' => $inputs['password']])) {
            return redirect()->route('panel.welcome');
        }
        return back()->with('error', 'Kullanıcı adı yada şifre yanlış');
    }

    public function logout()
    {
        \Auth::logout();
        return redirect()->route('panel.index');
    }

}
