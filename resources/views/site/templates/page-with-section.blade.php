@extends('site.layouts.site')

@section('content')

    @include('site.parts.hero', ['class'=>'head-image','parent' => $page])

    <!-- Section History  -->
    <section id="start">
        <div class="container m-top-70">
            <div class="row">
                @php($page_sections = $page->sections->where('type','<',\App\Enums\SectionType::Footer))
                @php($footer_section = $page->sections->where('type', \App\Enums\SectionType::Footer))
                @foreach($page_sections as $section)
                    <div class="col-md-12 m-bottom-70">
                        <div class="row">
                            @if($section->type === \App\Enums\SectionType::Tight && $section->image_direction === \App\Enums\ImageDirection::Left)

                                <div class="col-md-6 only-desktop">
                                    <img class="img-fluid bg-shadow" src="{{$section->image}}"
                                         alt="{{$section->title}}"/>
                                </div>

                                <div class="col-md-6">
                                    <div class="inner-box-2-right">
                                        <h3>{{$section->title_optional}}</h3>
                                        <h2>{{$section->title}}</h2>
                                        <img class="img-fluid only-mobile m-bottom-30 bg-shadow"
                                             src="{{$section->image}}" alt="{{$section->title}}"/>
                                        <p>{!! $section->content !!}</p>
                                    </div>
                                </div>

                            @elseif($section->type === \App\Enums\SectionType::Tight && $section->image_direction === \App\Enums\ImageDirection::Right)

                                <div class="col-md-6">
                                    <div class="inner-box-2-left">
                                        <h3>{{$section->title_optional}}</h3>
                                        <h2>{{$section->title}}</h2>
                                        <img class="img-fluid only-mobile m-bottom-30 bg-shadow"
                                             src="{{$section->image}}" alt="{{$section->title}}"/>
                                        <p>{!! $section->content !!}</p>
                                    </div>
                                </div>
                                <div class="col-md-6 only-desktop">
                                    <img class="img-fluid bg-shadow" src="{{$section->image}}"
                                         alt="{{$section->title}}"/>
                                </div>

                            @else

                                <div class="col-md-12">
                                    <img class="img-fluid bg-shadow" src="{{$section->image}}"
                                         alt="{{$section->title}}"/>
                                </div>
                                <div class="ml-auto col-md-10 mr-auto">
                                    <div class="inner-box-3">
                                        <h3>{{$section->title_optional}}</h3>
                                        <h2>{{$section->title}}</h2>
                                        <p>{!! $section->content !!}</p>
                                    </div>
                                </div>

                            @endif
                        </div>
                    </div>
                @endforeach

                @include('site.parts.discover',['section'=>$footer_section->first()])

            </div>
        </div>
    </section>
@endsection