<?php

namespace App\Http\Controllers\Site;

use App\Models\Event;
use App\Models\Page;
use Illuminate\Support\Facades\DB;

class EventController extends SiteBaseController
{
    public function events()
    {
        $page = Page::where('route', 'etkinlikler')->where('active', 1)->first();

        if ($page === null) {
            return redirect()->route('home');
        }

        $events = Event::where('active', 1)->latest()->get();

        return view('site.templates.events', compact('page', 'events'));
    }

    public function event($slug = null)
    {



        $page = Page::where('route', 'etkinlikler')->where('active', 1)->first();

        if ($page === null) {
            return redirect()->route('home');
        }

        $event = Event::with('images')->where('slug', $slug)->where('active', 1)->first();

        return view('site.templates.event', compact('page', 'event'));

    }
}
