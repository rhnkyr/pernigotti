@extends('admin.layouts.main')
@section('content')
    @include('admin.parts.breadcrumb',['page'=> 'Sayfa Bölümleri'])
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{$sections[0]->page->name}} Listesi
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-lg-12 m--align-right">
                            <a href="{{route('page.sections.create',['page'=>$sections[0]->page->route])}}"
                               class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-plus-circle"></i>
													<span>
													{{$sections[0]->page->name}} İçin Yeni Bölüm Ekle
													</span>
												</span>
                            </a>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet">
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="m-section__content">
                                <table class="table table-striped m-table">
                                    <thead>
                                    <tr>
                                        <th>
                                            Başlık
                                        </th>
                                        <th>
                                            Durum
                                        </th>
                                        <th>
                                            İşlemler
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sections as $section)
                                        <tr>
                                            <td style="width: 80%">
                                                @if($section->title)
                                                    {{$section->title}}
                                                @else
                                                    Son Bölüm
                                                @endif
                                            </td>
                                            <td>
                                                @if($section->active === 1)
                                                    <span class="m-badge  m-badge--success m-badge--wide">Aktif</span>
                                                @else
                                                    <span class="m-badge  m-badge--danger m-badge--wide">Pasif</span>
                                                @endif
                                            </td>
                                            <td style="width: 20%">
                                                <a href="{{route('page.sections.edit', ['page'=>$section->page->route, 'id'=>$section->id])}}"
                                                   class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                                   title="Düzenle">
                                                    <i class="la la-edit"></i>
                                                </a>
                                                @if($section->deletable === 1)
                                                    <a href="#" data-id="{{$section->id}}"
                                                       class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill del"
                                                       title="Sil">
                                                        <i class="la la-trash"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--end::Section-->
                    </div>
                    <!--end::Form-->
                </div>

            </div>
        </div>
    </div>
@endsection

@section('page_js')

    <script>

        $(function () {
            $('body').on('click', 'a.del', function (e) {
                e.preventDefault();
                var id = $(this).data('id');

                swal({
                    title: "Uyarı!",
                    text: "Silmek istediğinize emin misiniz?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: 'btn btn-danger',
                    confirmButtonText: "Evet",
                    cancelButtonText: "İptal"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: '{{route('pages.index')}}/' + id,
                            dataType: "json",
                            type: 'DELETE',
                            success: function (result) {
                                if (result.status)
                                    location.reload();
                            }
                        });
                    }
                });
            });
        });
    </script>
@endsection

