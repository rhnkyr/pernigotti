<!-- Section Chocolate -->
<section>
    <div class="container-fluid bg-chocolate-image" style="background-image: url('{{$sections[1]->image}}')">
        <div class="container no-padding">
            <div class="row">
                <div class="ml-auto col-md-10 mr-auto text-center color-white column">
                    <h2>{{$sections[1]->title}}</h2>
                    <p>{!! $sections[1]->content !!}</p>
                    <a class="btn-primary" href="{{route('products', ['slug' =>  $sections[1]->route])}}">TÜM
                        ÇİKOLATALAR</a>
                </div>
            </div>

            <div class="slider responsive">
                @foreach($chocolates as $chocolate)
                    <div>
                        <div class="product-slider-list chocolate-color">
                            <a href="{{route('product', ['category'=>$chocolate->category->parent_category->slug, 'slug'=>$chocolate->slug])}}">
                                <div class="product-pic-list">
                                    <img class="img-fluid" src="{{$chocolate->image}}"
                                         alt="{{$chocolate->category->name}} {{$chocolate->name}} {{$chocolate->weight}}g">
                                </div>
                                <div class="product-detail-list">
                                    <h3>{{$chocolate->name}}</h3>
                                    <p>{{$chocolate->weight}}g - {{$chocolate->category->name}} </p>
                                </div>
                                <div class="product-detail-button">
                                    <span>İNCELE</span>
                                    <i class="ion-android-arrow-forward"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="col-md-12">
                <div class="variable-controls">
                    <div class="row">
                        <div class="col-2 text-left">
                            <div class="variable-prev">
                                <i class="ion-ios-arrow-back"></i>
                            </div>
                        </div>

                        <div class="col-8">
                            <div class="slick-dots"></div>
                        </div>

                        <div class="col-2 text-right">
                            <div class="variable-next">
                                <i class="ion-ios-arrow-forward"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Section Ilgelato -->
<section>
    <div class="container-fluid bg-ilgelato-image" style="background-image: url('{{$sections[2]->image}}')">
        <div class="container">
            <div class="row">
                <div class="ml-auto col-md-10 mr-auto text-center color-white column">
                    <h2>{{$sections[2]->title}}</h2>
                    <p>{!! $sections[2]->content !!}</p>
                    <a class="btn-primary" href="{{route('products', ['slug' =>  $sections[2]->route])}}">TÜM IL
                        GELATO'LAR</a>
                </div>
            </div>

            <div class="slider responsive">
                @foreach($ilgelatos as $ilgelato)
                    <div>
                        <div class="product-slider-list ilgelato-color">
                            <a href="{{route('product', ['category'=>$ilgelato->category->parent_category->slug, 'slug'=>$ilgelato->slug])}}">
                                <div class="product-pic-list">
                                    <img class="img-fluid" src="{{$ilgelato->image}}"
                                         alt="{{$ilgelato->category->name}} {{$ilgelato->name}} {{$ilgelato->weight}}g">
                                </div>
                                <div class="product-detail-list">
                                    <h3>{{$ilgelato->name}}</h3>
                                    <p>{{$ilgelato->weight}}g - {{$ilgelato->category->name}} </p>
                                </div>
                                <div class="product-detail-button">
                                    <span>İNCELE</span>
                                    <i class="ion-android-arrow-forward"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="col-md-12">
                <div class="variable-controls">
                    <div class="row">
                        <div class="col-2 text-left">
                            <div class="variable-prev">
                                <i class="ion-ios-arrow-back"></i>
                            </div>
                        </div>

                        <div class="col-8">
                            <div class="slick-dots"></div>
                        </div>

                        <div class="col-2 text-right">
                            <div class="variable-next">
                                <i class="ion-ios-arrow-forward"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>