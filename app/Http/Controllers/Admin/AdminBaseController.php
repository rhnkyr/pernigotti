<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class AdminBaseController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth');
        if (!\Auth::check()) {
            redirect()->route('panel.index');
        }
    }

    public function clearCache()
    {
        \Artisan::call('page-cache:clear');
        \Artisan::call('cache:clear');
        \Artisan::call('optimize');

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('panel.welcome');

    }
}
