<!-- Section Recipe/Activity -->
<section style="margin-bottom: 0 !important;">
    <div class="container-fluid bg-two-color">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 bg-ilgelato-left">
                    <div class="recipe">
                        <h3>Güncel</h3>
                        <h2>IL Gelato Tarifleri</h2>
                    </div>


                    @foreach($recipes as $recipe)

                        <div class="recipe-list-home">
                            <a href="{{route('recipe', ['slug' => $recipe->slug])}}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="recipe-pic-list-home"
                                             style="background-image: url('{{$recipe->image}}')">
                                            <img class="img-fluid" src="{{asset('assets/images/icons/play.svg')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="recipe-detail-list-home">
                                            <h2>{{$recipe->name}}</h2>
                                            <p>{{$recipe->description}}</p>
                                            <div class="recipe-detail-button-home">
                                                İNCELE <i class="ion-android-arrow-forward"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                    @endforeach


                    <div class="col-md-12 text-center">
                        <a class="btn-primary" href="{{ route('recipes') }}">TÜM TARİFLER</a>
                    </div>
                </div>


                <div class="col-lg-5 bg-pernigotti-right">
                    <div class="activity">
                        <h3>Güncel</h3>
                        <h2>Pernigotti Etkinlikleri</h2>
                    </div>

                    @foreach($events as $event)
                        <div class="activity-list-home">
                            <a href="{{route('event', ['slug' => $event->slug])}}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="activity-pic-list-home">
                                            @if ($date = explode(' ', $event->when)) @endif
                                            <h2>{{$date[0]}}</h2>
                                            <h3>{{$date[1]}}</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="activity-detail-list-home">
                                            <h2>{{$event->name}}</h2>
                                            <p></p>
                                            <div class="activity-detail-button-home">
                                                İNCELE <i class="ion-android-arrow-forward"></i>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach

                    <div class="col-md-12 text-center">
                        <a class="btn-primary" href="{{ route('events') }}">TÜM ETKİNLİKLER</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>