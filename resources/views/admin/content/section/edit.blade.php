@extends('admin.layouts.main')
@section('content')
    <!-- BEGIN: Subheader -->
    @include('admin.parts.breadcrumb',['page'=> 'Bölüm Bilgileri'])
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Bilgiler
                                </h3>
                            </div>
                        </div>
                    </div>
                    {!! Form::model($section, ['route' => ['page.sections.update', 'page'=>$page , 'id'=>$section->id], 'class'=>'m-form', 'method'=>'PUT', 'files' => true]) !!}
                    <div class="m-portlet__body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if($section->deletable === 1)
                            <div class="form-group m-form__group col-lg-9">
                                {!! Form::label('route', 'Route (Rota - Slug) :'); !!}
                                @if($section->route)
                                    {!! Form::text('route', null, ['class' => 'form-control m-input']); !!}
                                @else
                                    {!! Form::text('route', null, ['class' => 'form-control m-input', 'placeholder'=> 'Herhangi bir içeriğe yönlendirme bulunmamaktadır!']); !!}
                                @endif
                            </div>
                        @endif
                        <div class="form-group m-form__group col-lg-3">
                            {!! Form::label('type', 'Görsel Tipi :'); !!}
                            {!! Form::select('type', [ '0' => 'Ana Sayfa', '1' => 'Kısa Görsel', '2' => 'Uzun Görsel', '3' => 'Sayfa Footer Görsel'], $section->type, ['class' => 'form-control m-input']); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-3">
                            {!! Form::label('image_direction', 'Görsel Yeri :'); !!}
                            {!! Form::select('image_direction', ['0'=> 'Sayfa Footer', '1' => 'Solda', '2' => 'Sağda'],$section->image_direction, ['class' => 'form-control m-input']); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-9">
                            {!! Form::label('image', 'Görsel :'); !!}
                            @if($section->image)
                                <div class="form-group m-form__group col-lg-9" style="padding-left: 0 !important;">
                                    <img src="{{$section->image}}" style="width: 600px"/>
                                </div>
                            @endif
                            {!! Form::file('image'); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-9">
                            {!! Form::label('title_optional', 'Footer Bölüm Üst Yazı :'); !!}
                            {!! Form::textarea('title_optional',null, ['class' => 'form-control m-input']); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-9">
                            {!! Form::label('title', 'Bölüm Başlık :'); !!}
                            {!! Form::textarea('title',null, ['class' => 'form-control m-input']); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-9">
                            {!! Form::label('content', 'Bölüm İçerik :'); !!}
                            {!! Form::textarea('content',null, ['class' => 'form-control m-input summernote']); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-3">
                            {!! Form::label('active', 'Durum :'); !!}
                            {!! Form::select('active', ['0' => 'Pasif', '1' => 'Aktif'],$section->active, ['class' => 'form-control m-input']); !!}
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <button class="btn btn-primary">
                                {!! Form::submit('Kaydet', ['class'=>'btn btn-primary']) !!}
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
@endsection
@section('page_js')
    <!--begin::Page Resources -->
    <script src="{{asset('assets/admin/assets/demo/default/custom/components/forms/widgets/summernote.js')}}"
            type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/lang/summernote-tr-TR.min.js"
            type="text/javascript"></script>
    <!--end::Page Resources -->
@endsection
