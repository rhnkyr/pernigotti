<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta name="HandheldFriendly" content="true"/>
    <title>{{$site_settings->site_title}}</title>

    <meta name="description" content="{{$site_settings->meta_description}}">
    <meta name="keywords" content="{{$site_settings->meta_keywords}}">
    <meta name="dcterms.dateCopyrighted" content="2018">
    <meta name="rating" content="All">
    <meta name="robots" content="index, follow">
    <meta name="googlebot" content="index, follow">
    <meta name="revisit-after" content="7 Days">

    <link rel="icon" href="{{asset('assets/images/favicon.ico')}}">

    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Normalize CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.min.css">

    <!-- Ionicon -->
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('assets/styles/style.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/responsive.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/menu.min.css')}}">

    <!-- Royal Slider CSS -->
    <link href="{{asset('assets/styles/royalslider.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/styles/rs-minimal-white.min.css')}}" rel="stylesheet">

    <!-- Slick Carousel Css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/styles/slick.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/styles/slick-theme.min.css')}}"/>

</head>

<body>

<div class="container">
    <!-- Header -->
    <nav class="navbar">

        @include('site.menus.top-menu')
        @include('site.menus.product-menu')

    </nav>
</div>

<!-- Content Wrapper -->
<div class="wrap" role="document">

@yield('content')

    @include('site.parts.instagram')
</div>

<!-- Footer -->
@include('site.parts.footer')


<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<!-- Optional JavaScript -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<!--<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>-->

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

<script src="{{asset('assets/javascript/jquery-1.8.3.min.js')}}"></script>
<script src="{{asset('assets/javascript/jquery.royalslider.min.js')}}"></script>
<script src="{{asset('assets/javascript/jquery.easing-1.3.min.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/javascript/megamenu.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/javascript/slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/javascript/custom.min.js')}}"></script>

@includeWhen($isHome,'site.scripts.slider')

@section('extjs')
@endsection

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-71711165-1', 'auto');
    ga('send', 'pageview');
</script>

</body>
</html>