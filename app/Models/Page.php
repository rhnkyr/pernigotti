<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 14 Apr 2018 09:26:42 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Page
 *
 * @property int $id
 * @property string $name
 * @property string $route
 * @property string $template
 * @property string $image
 * @property string $top_text
 * @property string $middle_text
 * @property string $description
 * @property string $page_description
 * @property int $deletable
 * @property int $active
 *
 * @package App\Models
 */
class Page extends Eloquent
{
    public $timestamps = false;

    protected $casts = [
        'deletable' => 'int',
        'active'    => 'int'
    ];

    protected $fillable = [
        'name',
        'route',
        'template',
        'image',
        'top_text',
        'middle_text',
        'description',
        'page_description',
        'deletable',
        'active'
    ];

    public function sections()
    {
        return $this->hasMany(PageSection::class, 'page_slug', 'route')->where('active', 1)->orderBy('order', 'ASC');
    }
}
