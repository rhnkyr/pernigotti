<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class PageController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::with('sections')->orderBy('name', 'asc')->get(['id', 'name', 'route', 'deletable']);
        return view('admin.content.page.list', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.content.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required',
            'route'    => 'required',
            'template' => 'required',
            'image'    => 'required',
            'active'   => 'required'
        ]);

        if ($request->file('image') !== null) {

            $file = $request->file('image');
            $name = str_slug($request->input('name')) . '.' . $file->getClientOriginalExtension();

            if (!\Storage::disk('uploads')->put('bg' . DIRECTORY_SEPARATOR . $name, \File::get($file))) {
                throw new HttpException(500);
            }

        }

        $page = new Page();

        $page->name             = $request->input('name');
        $page->route            = str_slug($page->name);
        $page->template         = $request->input('template');
        $page->image            = '/uploads/bg/' . $name;
        $page->top_text         = $request->input('top_text');
        $page->middle_text      = $request->input('middle_text');
        $page->description      = $request->input('description');
        $page->page_description = $request->input('page_description');
        $page->deletable        = 1;
        $page->active           = $request->input('active');

        $page->save();

        return redirect()->route('pages.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::find($id);

        if ($page === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return redirect()->route('pages.index');
        }

        return view('admin.content.page.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name'     => 'required',
            'route'    => 'required',
            'template' => 'required',
            'image'    => 'required',
            'active'   => 'required'
        ]);

        $page = Page::find($id);

        if ($page === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return redirect()->route('pages.index');
        }

        if ($request->file('image') !== null) {

            $file = $request->file('image');
            $n    = $request->input('name') !== null ? $page->name : $request->input('name');
            $name = str_slug($n) . '.' . $file->getClientOriginalExtension();

            if (!\Storage::disk('uploads')->put('bg' . DIRECTORY_SEPARATOR . $name, \File::get($file))) {
                throw new HttpException(500);
            }

        }


        if ($name !== null) {
            $page->image = '/uploads/bg/' . $name;
        }

        if ($page->deletable === 0) {

            $page->top_text         = $request->input('top_text');
            $page->middle_text      = $request->input('middle_text');
            $page->description      = $request->input('description');
            $page->page_description = $request->input('page_description');
            $page->active           = $request->input('active');

            $page->save();

        } else {

            $page->name             = $request->input('name');
            $page->top_text         = $request->input('top_text');
            $page->image            = $request->input('title');
            $page->top_text         = $request->input('top_text');
            $page->middle_text      = $request->input('middle_text');
            $page->description      = $request->input('description');
            $page->page_description = $request->input('page_description');
            $page->active           = $request->input('active');

            $page->save();

        }


        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('pages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return array
     */
    public function destroy($id)
    {
        $page = Page::where('deletable', 1)->find($id);

        if ($page === null) {
            return ['status' => false];
        }

        $page->delete();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return ['status' => true];
    }
}
