<?php

namespace App\Http\Controllers\Admin;

use App\Models\Faq;
use Illuminate\Http\Request;

class FaqController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs = Faq::orderBy('order', 'asc')->get(['id', 'title','active']);
        return view('admin.content.faq.list', compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.content.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'   => 'required|max:255',
            'content' => 'required',
            'active'  => 'required'
        ]);

        $faq          = new Faq();
        $faq->title   = $request->input('title');
        $faq->content = $request->input('content');
        $faq->active  = $request->input('active');

        $faq->save();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('faqs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq = Faq::find($id);

        if ($faq === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return redirect()->route('faqs.index');
        }

        return view('admin.content.faq.edit', compact('faq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'   => 'required|max:255',
            'content' => 'required',
            'active'  => 'required'
        ]);

        $faq = Faq::find($id);

        if ($faq === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return redirect()->route('faqs.index');
        }

        $faq->title   = $request->input('title');
        $faq->content = $request->input('content');
        $faq->active  = $request->input('active');

        $faq->save();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('faqs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return array
     */
    public function destroy($id)
    {
        $faq = Faq::find($id);

        if ($faq === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return ['status' => false];
        }

        $faq->delete();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return ['status' => true];
    }

    public function sort()
    {
        $i = 1;

        foreach ($_POST['item'] as $value) {

            $faq        = Faq::find($value);
            $faq->order = $i;
            $faq->save();

            $i++;
        }
        return ['status' => true];

    }
}
