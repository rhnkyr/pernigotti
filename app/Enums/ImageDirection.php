<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class ImageDirection extends Enum
{
    const Left = 1;
    const Right = 2;

    /**
     * Get the description for an enum value
     *
     * @param  int $value
     * @return string
     */
    public static function getDescription(int $value): string
    {
        switch ($value) {
            case self::Left:
                return 'Sağda';
                break;
            case self::Right:
                return 'Solda';
                break;
            default:
                return self::getKey($value);
        }
    }
}
