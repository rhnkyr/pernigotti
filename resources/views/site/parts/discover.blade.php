<div class="col-md-12 text-center discover column" style="background: url('{{$section->image}}') no-repeat;background-size: cover">
    <h3>{{$section->title_optional}}</h3>
    <h2>{{$section->title}}</h2>
    <p>{!! $section->content !!}</p>

    <div class="row">
        <div class="col-md-6 text-right">
            <a class="btn-primary" href="{{route('products', ['slug'=>'cikolata'])}}">TÜM ÇİKOLATALAR</a>
        </div>
        <div class="col-md-6 text-left">
            <a class="btn-primary" href="{{route('products', ['slug'=>'ilgelato'])}}">TÜM IL GELATO'LAR</a>
        </div>
    </div>
</div>