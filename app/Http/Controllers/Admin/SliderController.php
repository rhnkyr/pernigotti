<?php

namespace App\Http\Controllers\Admin;

use App\Models\Slide;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class SliderController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slide::orderBy('order', 'asc')->get(['id', 'image', 'top_text', 'active']);
        return view('admin.content.slide.list', compact('slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.content.slide.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \HttpException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image'       => 'required|image|mimes:jpeg,jpg,png',
            'top_text'    => 'required',
            'middle_text' => 'required',
            'description' => 'required',
            'route'       => 'required',
            'active'      => 'required'
        ]);

        if ($request->file('image') !== null) {

            $file = $request->file('image');
            $name = str_slug($request->input('top_text')) . '.' . $file->getClientOriginalExtension();

            if (!\Storage::disk('uploads')->put('slides' . DIRECTORY_SEPARATOR . $name, \File::get($file))) {
                throw new \HttpException(500);
            }

            $targetDir = public_path('uploads/slides');
            $path      = $targetDir . DIRECTORY_SEPARATOR . $name;

            $img = Image::make($path);

            $img->resize(1440, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path, 80);

        }

        $slide = new Slide();

        $slide->top_text    = $request->top_text;
        $slide->middle_text = $request->middle_text;
        $slide->image       = '/uploads/slides' . DIRECTORY_SEPARATOR . $name;
        $slide->description = $request->description;
        $slide->route       = $request->route;
        $slide->active      = $request->active;
        $slide->order       = 1;

        $slide->save();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('slides.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = Slide::find($id);

        if ($slide === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return redirect()->route('slides.index');
        }

        return view('admin.content.slide.edit', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \HttpException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image'       => 'nullable|image|mimes:jpeg,jpg,png',
            'top_text'    => 'required',
            'middle_text' => 'required',
            'description' => 'required',
            'route'       => 'required',
            'active'      => 'required'
        ]);

        if ($request->file('image') !== null) {

            $file = $request->file('image');
            $name = str_slug($request->input('top_text')) . '-' . str_random(5) . '.' . $file->getClientOriginalExtension();

            if (!\Storage::disk('uploads')->put('slides' . DIRECTORY_SEPARATOR . $name, \File::get($file))) {
                throw new \HttpException(500);
            }

            $targetDir = public_path('uploads/slides');
            $path      = $targetDir . DIRECTORY_SEPARATOR . $name;

            $img = Image::make($path);

            $img->resize(1440, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path, 80);

        }

        $slide = Slide::find($id);

        $slide->top_text    = $request->top_text;
        $slide->middle_text = $request->middle_text;
        if (isset($name)) {
            $slide->image = '/uploads/slides' . DIRECTORY_SEPARATOR . $name;
        }
        $slide->description = $request->description;
        $slide->route       = $request->route;
        $slide->active      = $request->active;

        $slide->save();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('slides.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = Slide::find($id);

        if ($slide === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return ['status' => false];
        }

        $slide->delete();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return ['status' => true];
    }

    public function sort()
    {
        $i = 1;

        foreach ($_POST['item'] as $value) {

            $faq        = Slide::find($value);
            $faq->order = $i;
            $faq->save();

            $i++;
        }
        return ['status' => true];

    }
}
