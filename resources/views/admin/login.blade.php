<!DOCTYPE html>

<html lang="tr" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>
        Pernigotti Yönetim Paneli
    </title>
    <meta name="robots" content="noindex" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <style>
        .btn-login{
            background: linear-gradient(133.15deg, #D0A349 0%, #F7CC75 100%); color: #fff;
        }
    </style>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="{{asset('/assets/admin/assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/assets/admin/assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
        <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
            <div class="m-stack m-stack--hor m-stack--desktop">
                <div class="m-stack__item m-stack__item--fluid">
                    <div class="m-login__wrapper">
                        <div class="m-login__logo">
                            <a href="#">
                                <img src="{{asset('assets/images/logos/pernigotti.svg')}}">
                            </a>
                        </div>
                        <div class="m-login__signin">
                            <div class="m-login__head">
                                <h3 class="m-login__title">
                                   Panel Giriş
                                </h3>
                            </div>
                            <form class="m-login__form m-form" action="{{route('login.check')}}" method="post">
                                {{ csrf_field() }}
                                @if (\Session::has('error'))
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span>{{Session::get('error')}}</span>
                                    </div>
                                @endif
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input" type="text" placeholder="Kullanıcı Adı" name="username" autocomplete="off">
                                </div>
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Şifre" name="password">
                                </div>
                                <!--<div class="row m-login__form-sub">
                                    <div class="col m--align-left">
                                        <label class="m-checkbox m-checkbox--focus">
                                            <input type="checkbox" name="remember">
                                            Remember me
                                            <span></span>
                                        </label>
                                    </div>
                                </div>-->
                                <div class="m-login__form-action">
                                    <button id="m_login_signin_submit" class="btn  m-btn--air btn-login">
                                       Giriş
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content" style="background-image: url({{asset('/assets/admin/assets/slide1.jpg')}})">
            <div class="m-grid__item m-grid__item--middle">
                <h3 class="m-login__welcome">
                    Pernigotti Web Site Yönetim Paneli
                </h3>
            </div>
        </div>
    </div>
</div>
<!-- end:: Page -->
<!--begin::Base Scripts -->
<script src="{{asset('/assets/admin/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('/assets/admin/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
<!--end::Base Scripts -->
<!--begin::Page Snippets -->
<script src="{{asset('/assets/admin/assets/snippets/pages/user/login.js')}}" type="text/javascript"></script>
<!--end::Page Snippets -->
</body>
<!-- end::Body -->
</html>
