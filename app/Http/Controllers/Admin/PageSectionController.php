<?php

namespace App\Http\Controllers\Admin;

use App\Models\PageSection;
use Http\Client\Exception\HttpException;
use Illuminate\Http\Request;

class PageSectionController extends AdminBaseController
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($page)
    {
        return view('admin.content.section.create', compact('page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Http\Client\Exception\HttpException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image'     => 'nullable|image|mimes:jpeg,jpg,png',
            'page_slug' => 'required',
            'type'      => 'required',
            'active'    => 'required'
        ]);

        if ($request->file('image') !== null) {

            $file = $request->file('image');
            $name = str_slug($request->input('name')) . '.' . $file->getClientOriginalExtension();

            if (!\Storage::disk('uploads')->put('sayfa' . DIRECTORY_SEPARATOR . $name, \File::get($file))) {
                throw new HttpException(500);
            }

        }

        $order = PageSection::where('page_slug', $request->input('page'))->max('order');

        $section                  = new PageSection();
        $section->page_slug       = $request->input('page');
        $section->type            = $request->input('type');
        $section->image           = isset($name) ? '/uploads/sayfa' . DIRECTORY_SEPARATOR . $name : null;
        $section->title_optional  = $request->input('title_optional');
        $section->title           = $request->input('title');
        $section->route           = $request->input('route');
        $section->content         = $request->input('content');
        $section->image_direction = $request->input('image_direction');
        $section->order           = $order + 1;
        $section->active          = $request->input('active');
        $section->deletable       = 1;

        $section->save();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('page.sections.show', ['page' => $request->input('page'), 'id' => 1]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($page, $id)
    {
        $sections = PageSection::with('page')->where('page_slug', $page)->orderBy('order', 'asc')->get();

        return view('admin.content.section.list', compact('sections'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($page, $id)
    {
        $section = PageSection::find($id);

        if ($section === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return redirect()->route('page.sections.show', ['page' => $page, 'id' => $id]);
        }

        return view('admin.content.section.edit', compact('section', 'page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $page
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function update(Request $request, $page, $id)
    {
        $this->validate($request, [
            'image'  => 'nullable|image|mimes:jpeg,jpg,png',
            'type'   => 'required',
            'active' => 'required'
        ]);

        if ($request->file('image') !== null) {

            $file = $request->file('image');
            $name = str_slug($request->input('name')) . '.' . $file->getClientOriginalExtension();

            if (!\Storage::disk('uploads')->put('sayfa' . DIRECTORY_SEPARATOR . $name, \File::get($file))) {
                throw new HttpException(500);
            }

        }

        $section                 = PageSection::find($id);
        $section->page_slug      = $request->input('page') ?? $section->page_slug;
        $section->type           = $request->input('type');
        $section->image          = isset($name) ? '/uploads/sayfa' . DIRECTORY_SEPARATOR . $name : null;
        $section->title_optional = $request->input('title_optional');
        $section->title          = $request->input('title');
        if ($section->deletable === 1) {
            $section->route = $request->input('route');
        }
        $section->content         = $request->input('content');
        $section->image_direction = $request->input('image_direction');
        $section->active          = $request->input('active');
        $section->deletable       = 1;

        $section->save();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('page.sections.show', ['page' => $page, 'id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $page
     * @param  int $id
     * @return array
     */
    public function destroy($page, $id)
    {
        $section = PageSection::where('deletable', 1)->find($id);

        if ($section === null) {
            return ['status' => false];
        }

        $section->delete();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return ['status' => true];
    }

    /*public function sort()
    {
        $i = 1;

        foreach ($_POST['item'] as $value) {

            $faq        = PageSection::find($value);
            $faq->order = $i;
            $faq->save();

            $i++;
        }
        return ['status' => true];

    }*/
}
