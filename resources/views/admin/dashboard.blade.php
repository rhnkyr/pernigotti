@extends('admin.layouts.main')
@section('content')
    <!-- BEGIN: Subheader -->
    @include('admin.parts.breadcrumb',['page'=> 'Anasayfa'])
    <div class="m-content">
        <div class="col-lg-12">
            <div class="m-alert m-alert--icon m-alert--outline alert alert-danger" role="alert">
                <div class="m-alert__icon">
                    <i class="la la-exclamation-circle"></i>
                </div>
                <div class="m-alert__text">
                    Her değişiklikten sonra Cache Temizle işlemini gerçekleştiriniz! Aksi takdirde değişiklikler sayfaya yansımayabilir. Cache sitenin daha hızlı çalışması için yapılan işlemdir.
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="m-alert m-alert--icon m-alert--outline alert alert-primary" role="alert">
                <div class="m-alert__icon">
                    <i class="la la-info-circle"></i>
                </div>
                <div class="m-alert__text">
                    Yönetim Paneline Hoş Geldiniz. Yapmak istediğiniz işlemi soldaki menüden seçebilirsiniz
                </div>
            </div>
        </div>
    </div>
@endsection
