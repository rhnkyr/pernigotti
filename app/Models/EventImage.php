<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 14 Apr 2018 09:27:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EventImage
 * 
 * @property int $id
 * @property string $event_id
 * @property string $path
 * @property string $thumb_path
 * @property int $active
 *
 * @package App\Models
 */
class EventImage extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'active' => 'int'
	];

	protected $fillable = [
		'event_id',
		'path',
		'thumb_path',
		'active'
	];

    public function event()
    {
        return $this->hasOne(Event::class,'id','event_id');
    }
}
