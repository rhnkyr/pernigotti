@extends('admin.layouts.main')
@section('content')
    @include('admin.parts.breadcrumb',['page'=> 'Sayfa Bölümleri'])
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{$sections[0]->page->name}} Listesi
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-lg-8">
                            <div class="m-alert m-alert--icon m-alert--outline alert alert-primary" role="alert">
                                <div class="m-alert__icon">
                                    <i class="la la-info-circle"></i>
                                </div>
                                <div class="m-alert__text">
                                    Sürükle bırak ile sıralamayı değiştirebilirsiniz.
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 m--align-right">
                            <a href="{{route('page.sections.create',['page'=>$sections[0]->page->route])}}"
                               class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-question-circle"></i>
													<span>
													{{$sections[0]->page->name}} İçin Yeni Bölüm Ekle
													</span>
												</span>
                            </a>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div>

                <div class="row" id="m_sortable_portlets">
                    <div class="col-lg-12">
                        @foreach($sections as $section)
                            <div class="m-portlet m-portlet--mobile m-portlet--sortable" id="item-{{$section->id}}">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <h3 class="m-portlet__head-text">
                                                @if($section->title)
                                                    {{$section->title}}
                                                @else
                                                    Son Bölüm
                                                @endif
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="m-portlet__head-tools">
                                        <ul class="m-portlet__nav">
                                            <li class="m-portlet__nav-item">
                                                <a href="{{route('page.sections.edit', ['page'=>$section->page->route, 'id'=>$section->id])}}"
                                                   class="m-portlet__nav-link btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                                    <i class="la la-edit"></i>
                                                </a>
                                            </li>
                                            @if($section->deletable === 1)
                                                <li class="m-portlet__nav-item">
                                                    <a href="#" data-id="{{$section->id}}"
                                                       class="m-portlet__nav-link btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill del">
                                                        <i class="la la-trash"></i>
                                                    </a>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_js')
    <!--begin::Page Vendors -->
    <script src="{{asset('assets/admin/assets/vendors/custom/jquery-ui/jquery-ui.bundle.js')}}"
            type="text/javascript"></script>
    <!--end::Page Vendors -->
    <script>

        $(function () {
            $('body').on('click', 'a.del', function (e) {
                e.preventDefault();
                var id = $(this).data('id');

                swal({
                    title: "Uyarı!",
                    text: "Silmek istediğinize emin misiniz?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: 'btn btn-danger',
                    confirmButtonText: "Evet",
                    cancelButtonText: "İptal"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: '/admin-panel/page/{{$sections[0]->page->route}}/sections/' + id,
                            dataType: "json",
                            type: 'DELETE',
                            success: function (result) {
                                if (result.status)
                                    location.reload();
                            }
                        });
                    }
                });
            });

            $("#m_sortable_portlets").sortable({
                connectWith: ".m-portlet__head",
                items: ".m-portlet",
                opacity: 0.8,
                handle: '.m-portlet__head',
                coneHelperSize: true,
                placeholder: 'm-portlet--sortable-placeholder',
                forcePlaceholderSize: true,
                tolerance: "pointer",
                helper: "clone",
                cancel: ".m-portlet--sortable-empty", // cancel dragging if portlet is in fullscreen mode
                revert: 250, // animation in milliseconds
                update: function (b, c) {
                    if (c.item.prev().hasClass("m-portlet--sortable-empty")) {
                        c.item.prev().before(c.item);
                    }

                    var data = $('#m_sortable_portlets').sortable('serialize')+'&page={{$sections[0]->page->route}}';

                    $.ajax({
                        data: data,
                        dataType: "json",
                        type: 'POST',
                        url: '{{ route('sections.sort') }}',
                        success: function (result) {
                            if (result.status) {
                                swal({
                                    title: 'Bilgi',
                                    text: 'İşleminiz gerçekleştirildi',
                                    timer: 2000,
                                    type: 'success',
                                    showConfirmButton: false
                                });
                            }

                        }
                    });
                }
            });

        });
    </script>
@endsection

