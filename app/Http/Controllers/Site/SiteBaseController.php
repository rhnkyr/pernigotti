<?php
/**
 * Created by PhpStorm.
 * User: erhankayar
 * Date: 14.04.2018
 * Time: 14:09
 */

namespace App\Http\Controllers\Site;


use App\Http\Controllers\Controller;
use App\Models\MenuElement;
use App\Models\ProductCategory;
use App\Models\SiteSetting;
use Vinkla\Instagram\Instagram;

class SiteBaseController extends Controller
{

    public $isHome = false;

    public function __construct()
    {
        $site_settings = SiteSetting::first();

        $menu_elements = MenuElement::where('active', 1)->get();

        $top = $menu_elements->filter(function ($item) {
            return $item['menu_section'] === 'top';
        })->sortBy('order');

        $categories = ProductCategory::where('parent_id', 0)->with(['sub_categories'])->get();

        $footer = $menu_elements->filter(function ($item) {
            return $item['menu_section'] === 'footer';
        })->sortBy('order');

        view()->share('top_menu_elements', $top);
        view()->share('product_menu_elements', $categories);
        view()->share('footer_menu_elements', $footer);
        view()->share('isHome', $this->isHome);
        view()->share('site_settings', $site_settings);

        try {
            $instagram        = new Instagram(env('INSTAGRAM_TOKEN'));
            $instagram_images = $instagram->get();
            $instagram_images = collect($instagram_images)->take(12);
            view()->share('instagram_images', $instagram_images);
        } catch (\Exception $e) {
            // left empty intentionally
            \Log::error('Instagram API token error');
            view()->share('instagram_images', []);
        }
    }
}