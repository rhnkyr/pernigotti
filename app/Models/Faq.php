<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 19 Apr 2018 17:35:53 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Faq
 * 
 * @property int $id
 * @property string $title
 * @property string $content
 * @property int $active
 * @property int $order
 *
 * @package App\Models
 */
class Faq extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'active' => 'int',
		'order' => 'int'
	];

	protected $fillable = [
		'title',
		'content',
		'active',
		'order'
	];
}
