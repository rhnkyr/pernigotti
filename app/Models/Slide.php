<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 12 Apr 2018 13:04:53 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Slide
 * 
 * @property int $id
 * @property string $image
 * @property string $top_text
 * @property string $middle_text
 * @property string $description
 * @property string $route
 * @property int $order
 * @property int $active
 *
 * @package App\Models
 */
class Slide extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'order' => 'int',
		'active' => 'int'
	];

	protected $fillable = [
		'image',
		'top_text',
		'middle_text',
		'description',
		'route',
		'order',
		'active'
	];
}
