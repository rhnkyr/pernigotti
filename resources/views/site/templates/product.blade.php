@extends('site.layouts.site')

@section('content')

    @include('site.parts.hero', ['class'=>'product-head-image','parent' => $page,'child'=> $product])

    <!-- Section Product Details-->
    <section>
        <div class="container-fluid no-repeat">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 product-detail">
                        <h1>{{ucfirst(mb_strtolower($product->category->name))}}</h1>
                        <h2>{{$product->name}} {{$product->weight}}g</h2>
                        <img class="img-fluid only-mobile m-bottom-30" src="{{$product->image}}" alt="{{$product->category->name}} {{$product->name}} {{$product->weight}}g">
                        <p>{{$product->description}}</p>
                        <h3>İçindekiler</h3>
                        <p>{{$product->ingredients}}</p>
                    </div>

                    <div class="col-md-6 text-center product-image only-desktop">
                        <img class="img-fluid" src="{{$product->image}}" alt="{{$product->category->name}} {{$product->name}} {{$product->weight}}g">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Section Next/Prev Product-->
    <section>
        <div class="container-fluid no-repeat">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 recommended-product">
                        <a href="{{route('product', ['category'=> $previous->category->parent_category->slug, 'slug'=> $previous->slug])}}">
                            <h2>{{$previous->name}}</h2>
                            <p>{{$previous->weight}}g - {{ucfirst(mb_strtolower($previous->category->name))}}</p>
                            <div class="product-detail-button">
                                <span>İNCELE</span>
                                <i class="ion-android-arrow-forward"></i>
                            </div>
                            <div class="recommended-product-bg" style="background-image: url('{{$previous->image}}')"></div>
                        </a>
                    </div>

                    <div class="col-md-6 recommended-product">
                        <a href="{{route('product', ['category'=> $next->category->parent_category->slug, 'slug'=> $next->slug])}}">
                            <h2>{{$next->name}}</h2>
                            <p>{{$next->weight}}g - {{ucfirst(mb_strtolower($next->category->name))}}</p>
                            <div class="product-detail-button">
                                <span>İNCELE</span>
                                <i class="ion-android-arrow-forward"></i>
                            </div>
                            <div class="recommended-product-bg" style="background-image: url('{{$next->image}}')"></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Section Similar -->
    <section>
        <div class="container-fluid bg-similar-color">
            <div class="container no-padding">
                <div class="row">
                    <div class="col-8 color-similar">
                        <h2>Benzer Diğer Çikolata Ürünleri</h2>
                    </div>
                    <!--<div class="col-4 color-similar">
                        <a class="" href="{{route('products', ['slug' => $product->category->parent_category->slug])}}">TÜMÜ <i class="ion-ios-arrow-forward"></i></a>
                    </div>-->
                </div>

                <div class="slider responsive">
                    @foreach($all_products as $rp)
                    <div>
                        <div class="product-slider-list chocolate-color">
                            <a href="{{route('product',['category'=> $rp->category->parent_category->slug, 'slug'=>$rp->slug])}}">
                                <div class="product-pic-list">
                                    <img class="img-fluid" src="{{$rp->image}}" alt="{{$rp->category->name}} {{$rp->name}} {{$rp->weight}}g">
                                </div>
                                <div class="product-detail-list">
                                    <h3>{{$rp->name}}</h3>
                                    <p>{{$rp->weight}}g - {{ucfirst(mb_strtolower($rp->category->name))}}</p>
                                </div>
                                <div class="product-detail-button">
                                    <span>İNCELE</span>
                                    <i class="ion-android-arrow-forward"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach

                </div>

                <div class="col-md-12">
                    <div class="variable-controls">
                        <div class="row">
                            <div class="col-2 text-left">
                                <div class="variable-prev">
                                    <i class="ion-ios-arrow-back"></i>
                                </div>
                            </div>

                            <div class="col-8">
                                <div class="slick-dots"></div>
                            </div>

                            <div class="col-2 text-right">
                                <div class="variable-next">
                                    <i class="ion-ios-arrow-forward"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection