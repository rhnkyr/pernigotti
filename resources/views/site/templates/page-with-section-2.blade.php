@extends('site.layouts.site')

@section('content')

    @include('site.parts.hero', ['class'=>'head-image','parent' => $page])

    @php($footer_section = $page->sections->where('type', \App\Enums\SectionType::Footer))
    <!-- Section History  -->
    <section id="start">
        <div class="container">

            <div class="row">
                <div class="ml-auto col-md-10 mr-auto text-center column-2 color-brown">
                    {!! $page->page_description !!}
                </div>
            </div>

            <div class="row">
                @foreach($page->sections as $section)
                    <div class="col-md-12 m-bottom-70">
                        <div class="row">
                            @if($section->type === \App\Enums\SectionType::Tight && $section->image_direction === \App\Enums\ImageDirection::Left)


                                <div class="ml-auto col-md-10 mr-auto m-bottom-70">
                                    <div class="row">
                                        <div class="col-md-8 only-desktop">
                                            <img class="img-fluid bg-shadow" src="{{$section->image}}" alt="{{$section->title}}"/>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="inner-box-right">
                                                <h3>{{$section->title}}</h3>
                                                <img class="img-fluid only-mobile m-bottom-30 bg-shadow"
                                                     src="{{$section->image}}" alt="{{$section->title}}"/>
                                                <p>{!! $section->content !!}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @elseif($section->type === \App\Enums\SectionType::Tight && $section->image_direction === \App\Enums\ImageDirection::Right)

                                <div class="ml-auto col-md-10 mr-auto">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="inner-box-left">
                                                <h3>{{$section->title}}</h3>

                                                <img class="img-fluid only-mobile m-bottom-30 bg-shadow"
                                                     src="{{$section->image}}" alt="{{$section->title}}"/>
                                                <p>{!! $section->content !!}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-8 only-desktop">
                                            <img class="img-fluid bg-shadow" src="{{$section->image}}"
                                                 alt="{{$section->title}}"/>
                                        </div>
                                    </div>
                                </div>

                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>


    @if($footer_section !== null)

    <section>
        <div class="container-fluid bg-similar-color">
            <div class="container no-padding">
                <div class="row ">
                    <div class="ml-auto col-10 mr-auto bg-shadow-2 text-center without-things">
                        {!! $footer_section->first()->content !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

    @endif

@endsection