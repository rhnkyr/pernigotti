<footer class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <a href="{{route('home')}}">
                    <img class="img-fluid logo" src="{{asset('assets/images/logos/pernigotti.svg')}}" alt="Pernigotti"/>
                </a>
            </div>

            @foreach($product_menu_elements as $category)

            <div class="col-md-2">
                <a href="{{route('products', ['slug' => $category->slug])}}">
                    <h6>{{$category->name}}</h6>
                </a>

                @foreach($category->sub_categories as $sub_category)

                <a href="{{route('products', ['slug' => $category->slug])}}#{{$sub_category->slug}}">
                    {{ucfirst(mb_strtolower($sub_category->name))}}
                </a>

                @endforeach

            </div>

            @endforeach

            <div class="col-md-2">
                <h6 class="footer-opacity">PERNİGOTTİ</h6>
                @foreach($top_menu_elements as $tm)
                    <a href="{{route('page', ['slug' => $tm->page_route])}}">
                        {{ $tm->name }}
                    </a>
                @endforeach
            </div>

            <div class="col-md-2">
                <h6 class="footer-opacity">İLETİŞİM</h6>

                <a href="{{route('contact')}}">
                    İletişim Bilgileri
                </a>
            </div>

            <div class="col-md-2 social-media">
                <h6 class="footer-opacity">SOSYAL MEDYA</h6>

                <ul>
                    <li>
                        <a href="{{$site_settings->facebook}}" target="_blank">
                            <i class="ion-social-facebook"></i>
                        </a>
                    </li>

                    <li>
                        <a href="{{$site_settings->youtube}}" target="_blank">
                            <i class="ion-social-youtube-outline"></i>
                        </a>
                    </li>

                    <li>
                        <a href="{{$site_settings->instagram}}" target="_blank">
                            <i class="ion-social-instagram-outline"></i>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 footer-sss">
                    <a href="{{route('sss')}}">
                        SSS
                    </a>
                    <a href="{{$site_settings->privacy}}" target="_blank">
                        Gizlilik Sözleşmesi
                    </a>
                    <a href="{{$site_settings->legal}}" target="_blank">
                        Yasal Uyarı
                    </a>
                </div>
                <div class="col-lg-4 text-center gelato-factory">
                    <img class="img-fluid" src="{{asset('assets/images/logos/chocolate-factory.png')}}" alt="Chocolate Factory"/>
                </div>
                <div class="col-lg-4 text-right footer-logos">
                    <a href="http://www.sansetgida.com.tr/" target="_blank">
                        <img class="img-fluid" src="{{asset('assets/images/logos/sanset.png')}}" alt="Sanset Gıda"/>
                    </a>
                    <a href="http://www.toksozgrup.com.tr/" target="_blank">
                        <img class="img-fluid" src="{{asset('assets/images/logos/toksozgrup.png')}}" alt="Toksöz Grup"/>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>