<?php

namespace App\Http\Controllers\Site;


use App\Models\Page;
use App\Models\Recipe;

class RecipeController extends SiteBaseController
{

    public function recipes()
    {
        $page = Page::where('route', 'tarifler')->where('active', 1)->first();

        if ($page === null) {
            return redirect()->route('home');
        }

        $recipes = Recipe::where('active', 1)->latest()->get();

        return view('site.templates.recipes', compact('page', 'recipes'));
    }

    public function recipe($slug = null)
    {

        $page = Page::where('route', 'tarifler')->where('active', 1)->first();

        if ($page === null) {
            return redirect()->route('home');
        }

        $recipe         = Recipe::where('slug', $slug)->where('active', 1)->first();
        $random_recipes = Recipe::inRandomOrder()->where('slug', '!=', $slug)->where('active', 1)->limit(3)->get();

        return view('site.templates.recipe', compact('page', 'recipe', 'random_recipes'));

    }

}
