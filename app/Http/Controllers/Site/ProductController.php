<?php

namespace App\Http\Controllers\Site;

use App\Models\Page;
use App\Models\Product;
use App\Models\ProductCategory;

class ProductController extends SiteBaseController
{
    //

    public function products($slug = null)
    {
        $page = Page::where('route', $slug)->where('active', 1)->first();

        if ($page === null) {
            return redirect()->route('home');
        }

        $main_category = ProductCategory::where('slug', $slug)->first();
        $categories    = $main_category->sub_categories()->with('ordered_products')->get();

        return view('site.templates.products', compact('page', 'categories'));
    }

    public function product($category = null, $slug = null)
    {
        $page = Page::where('route', $category)->where('active', 1)->first();

        if ($page === null) {
            return redirect()->route('home');
        }

        $product = Product::where('slug', $slug)->with('category')->first();

        if ($product === null) {
            return redirect()->route('home');
        }

        $previous = $product->previous($product->category_slug);
        if ($previous === null) {
            $previous = $product->last($product->category_slug);
        }
        $next = $product->next($product->category_slug);
        if ($next === null) {
            $next = $product->first($product->category_slug);
        }


        //$random_products = Product::inRandomOrder()->where('category_slug', $product->category_slug)->where('slug', '!=', $slug)->where('active', 1)->with('category')->limit(3)->get();
        $all_products = Product::where('category_slug', $product->category_slug)->where('slug', '!=', $slug)->where('active', 1)->with('category')->orderBy('order','asc')->get();

        return view('site.templates.product', compact('page', 'product', 'previous', 'next', 'all_products'));
    }
}
