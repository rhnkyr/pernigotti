<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.content.user.list', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.content.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required|max:255',
            'username' => 'required|max:255',
            'password' => 'required|max:255'
        ]);

        $check = User::where('username', $request->username)->first();

        if ($check !== null) {
            \Session::flash('error', 'Bu kullanıcı adı ile daha önce bir kullanıcı yaratılmış');

            return redirect()->route('users.index');
        }

        $user           = new User();
        $user->name     = $request->name;
        $user->username = $request->username;
        $user->password = \Hash::make($request->password);

        $user->save();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        if ($user === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return redirect()->route('users.index');
        }

        return view('admin.content.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'     => 'required|max:255',
            'username' => 'required|max:255'
        ]);

        $user           = User::find($id);
        $user->name     = $request->name;
        $user->username = $request->username;
        if ($request->password) {
            $user->password = \Hash::make($request->password);
        }

        $user->save();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if ($user === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return ['status' => false];
        }

        $user->delete();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return ['status' => true];
    }
}
