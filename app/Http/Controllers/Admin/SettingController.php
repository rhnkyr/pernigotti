<?php

namespace App\Http\Controllers\Admin;

use App\Models\SiteSetting;
use Illuminate\Http\Request;

class SettingController extends AdminBaseController
{


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $settings = SiteSetting::find($id);

        return view('admin.content.setting.edit', compact('settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'site_title'       => 'required|max:255',
            'facebook'         => 'required|max:255',
            'youtube'          => 'required|max:255',
            'instagram'        => 'required|max:255',
            'privacy'          => 'required|max:255',
            'legal'            => 'required|max:255',
            'meta_description' => 'required',
            'meta_keywords'    => 'required'
        ]);


        $setting = SiteSetting::find($id);

        $setting->site_title       = $request->site_title;
        $setting->facebook         = $request->facebook;
        $setting->youtube          = $request->youtube;
        $setting->instagram        = $request->instagram;
        $setting->meta_description = $request->meta_description;
        $setting->meta_keywords    = $request->meta_keywords;
        $setting->privacy          = $request->privacy;
        $setting->legal            = $request->legal;

        $setting->save();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('site-settings.edit', ['id' => $id]);
    }

}
