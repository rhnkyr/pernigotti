/*global $ */
$(document).ready(function () {
    "use strict";

    $('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');
    // Checks if li has sub (ul) and adds class for toggle icon - just an UI

    $('.menu > ul > li > ul:not(:has(ul))').addClass('normal-sub');
    // Checks if drodown menu's li elements have anothere level (ul), if not the dropdown is shown as regular dropdown, not a mega menu (thanks Luka Kladaric)

    $(".menu > ul > li:not(.menu-dup)").hover(
        function (e) {
            if ($(window).width() > 1025) {
                $(this).children("ul").fadeIn(150);
                e.preventDefault();
            }
        }, function (e) {
            if ($(window).width() > 1025) {
                $(this).children("ul").fadeOut(150);
                e.preventDefault();
            }
        }
    );
    // If width is more than 943px dropdowns are displayed on hover

    $(".menu > ul > li:not(.menu-dup) > a").click(function(e) {
        if ($(window).width() < 1025) {
            e.preventDefault();
            $(this).next("ul").fadeToggle(150);
        }
    });
    // If width is less or equal to 943px dropdowns are displayed on click (thanks Aman Jain from stackoverflow)

    $(".menu-mobile > .hamburger-menu").click(function (e) {
        $('.menu > ul').toggleClass('show-on-mobile');
        $('.menu').toggleClass('open');
        $(this).find('.bar').toggleClass('animate');

        e.preventDefault();
    });
    // when clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story (thanks mwl from stackoverflow)

    if( $(window).width() > 1025 ) {
        $('.menu > ul > li > ul > li').mouseover(function() {
            var _height = $(this).find('ul').height() + 4;

            $('.menu > ul > li > ul').css({'height' : (_height < 410 ? 410 : _height)+'px'});

            if(_height < 410){
                $(this).find('ul').css({'height' : '406px'});
            }
        });
    }
});
