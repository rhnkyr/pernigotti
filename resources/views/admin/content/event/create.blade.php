@extends('admin.layouts.main')
@section('content')
    <!-- BEGIN: Subheader -->
    @include('admin.parts.breadcrumb',['page'=> 'Etkinlikler'])
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Etkinlik Bilgiler
                                </h3>
                            </div>
                        </div>
                    </div>
                    {!! Form::open(['route' => 'events.store', 'class'=>'m-form']) !!}
                    <div class="m-portlet__body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form-group m-form__group col-lg-6">
                            {!! Form::label('name', 'Etkinlik Adı :'); !!}
                            {!! Form::text('name', null, ['class' => 'form-control m-input event_name']); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-6">
                            {!! Form::label('when', 'Zaman Aralığı :'); !!}
                            {!! Form::text('when', null, ['class' => 'form-control m-input']); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-6">
                            {!! Form::label('active', 'Durum :'); !!}
                            {!! Form::select('active', ['0' => 'Pasif', '1' => 'Aktif'],null, ['class' => 'form-control m-input']); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-12">
                            {!! Form::label('image', 'Görseller :'); !!}

                            <label class="form-control-label">Görseller (Eklenme sırasına göre uyglamada
                                sıralanır)</label>
                            <div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.
                            </div>
                            <br/>
                            <div id="container">
                                <a id="pickfiles" href="javascript:;" class="btn btn-sm btn-primary">Görsel
                                    Seçiniz</a>
                            </div>
                            <div class="uploaded"></div>

                        </div>

                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <button class="btn btn-primary">
                                {!! Form::submit('Kaydet', ['class'=>'btn btn-primary']) !!}
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
@endsection
@section('page_js')

    <script src="{{ asset('vendor/plupload/js/plupload.full.min.js') }}"></script>
    <script>

        $(function () {

            var uploader = new plupload.Uploader({
                runtimes: 'html5',
                browse_button: 'pickfiles', // you can pass an id...
                container: document.getElementById('container'), // ... or DOM Element itself
                url: '{{ route('events.upload') }}',
                chunk_size: '500kb',
                max_retries: 3,
                filters: {
                    max_file_size: '10mb',
                    mime_types: [
                        {title: "Görselle", extensions: "jpg,png,jpeg"}
                    ]
                },
                multipart_params: {
                    "event_name": $('.event_name').val(),
                    '_token': '{{ csrf_token() }}'
                },
                unique_names: true,
                init: {
                    PostInit: function () {
                        document.getElementById('filelist').innerHTML = '';
                    },

                    FilesAdded: function (up, files) {
                        //uploader.multipart_params.event_name = $('.event_name').val();
                        //plupload.multipart_parms['event_name'] = $('.event_name').val();
                        plupload.each(files, function (file) {
                            document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                        });
                        uploader.start();
                    },

                    FileUploaded: function (up, file, info) {
                        var ind = Math.floor((Math.random() * 100) + 1);
                        var obj = JSON.parse(info.response);

                        var tpl = "";
                        tpl += '<input type="hidden" name="images[]" id="url' + ind + '" value="' + obj.result.cleanFileName + '">';
                        tpl += '<img id="uploaded' + ind + '" src="/uploads/etkinlik/thumb_' + obj.result.cleanFileName + '" style="width: 300px"/>';
                        tpl += '<button id="b' + ind + '" class="btn btn-sm btn-danger del_multi" type="button">';
                        tpl += '<i class="fa fa-trash"></i>';
                        //tpl += '<span>Sil</span>';
                        tpl += '</button>';
                        $(".uploaded").append(tpl);

                        document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = 'Görsel yüklendi';
                    },

                    UploadProgress: function (up, file) {
                        document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>Görsel yüklenirken lütfen bekleyiniz...</span>';
                    },

                    Error: function (up, err) {
                        console.log("\nError #" + err.code + ": " + err.message);
                    },

                    UploadComplete: function (up, files) {
                        $("div#filelist").html('');
                    }
                }
            });

            uploader.bind('BeforeUpload', function(up, file) {
                up.settings.multipart_params = {

                    "event_name": $('.event_name').val(),
                    '_token': '{{ csrf_token() }}'

                };
            });

            uploader.init();

            $(document).on("click", ".del_multi", function () {
                var self = $(this), u = self.prevAll(":hidden").val();
                swal({
                    title: "Uyarı!",
                    text: "Silmek istediğinize emin misiniz?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: 'btn btn-danger',
                    confirmButtonText: "Evet",
                    cancelButtonText: "İptal"
                }).then(function (result) {
                    if (result.value) {
                        $.post("{{route('event.image.delete')}}", {url: u}, function (r) {
                            if (r.status) {
                                $("div#filelist").html('');
                                self.prev("img").remove();
                                self.prev(":hidden").remove();
                                self.remove();
                            } else
                                alert(r.message);
                        }, "json");
                    }
                });
                return false;
            });

        });

    </script>
@endsection
