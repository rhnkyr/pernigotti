@extends('site.layouts.site')

@section('content')

    @include('site.parts.hero', ['class'=>'head-image','parent' => $page])


    <!-- Section Activity  -->
    <section id="start">
        <div class="container m-bottom-100 m-top-70">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 color-brown m-bottom-30">
                            <h2>Tarifler</h2>
                        </div>

                        @foreach($recipes as $recipe)
                        <div class="col-md-12">
                            <a href="{{route('recipe', ['slug'=>$recipe->slug])}}" class="recipe-list">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="recipe-pic-list" style="background-image: url('{{$recipe->image}}')">
                                            <img class="img-fluid" src="{{asset('assets/images/icons/play.svg')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="recipe-detail-list">
                                            <h1>{{$recipe->name}}</h1>
                                            <p>{{$recipe->description}}</p>
                                            <div class="recipe-detail-button">
                                                İNCELE <i class="ion-android-arrow-forward"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection