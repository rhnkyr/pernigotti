<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ProductController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories = ProductCategory::with('ordered_products')->where('parent_id', '!=', 0)->get();
        return view('admin.content.products.list', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = ProductCategory::where('parent_id', '!=', 0)->get()->pluck('name', 'slug');
        return view('admin.content.products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \HttpException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_slug' => 'required|max:255',
            'name'          => 'required|max:255',
            'image'         => 'required|image|mimes:jpeg,jpg,png',
            'weight'        => 'required|max:16',
            'description'   => 'required',
            'ingredients'   => 'required',
            'active'        => 'required'
        ]);

        $current_category = ProductCategory::where('slug', $request->category_slug)->first();
        $parent           = $current_category->parent_category;

        if ($request->file('image') !== null) {

            $file = $request->file('image');
            $name = str_slug($request->category_slug . ' ' . $request->name . ' ' . $request->weight . 'g') . '.' . $file->getClientOriginalExtension();

            if (!\Storage::disk('uploads')->put('urunler' . DIRECTORY_SEPARATOR . $parent->slug .
                DIRECTORY_SEPARATOR . $request->category_slug . DIRECTORY_SEPARATOR . $name, \File::get($file))) {
                throw new \HttpException(500);
            }

            $targetDir = public_path('uploads/urunler' . DIRECTORY_SEPARATOR . $parent->slug . DIRECTORY_SEPARATOR . $request->category_slug);
            $path      = $targetDir . DIRECTORY_SEPARATOR . $name;
            $path2     = $targetDir . DIRECTORY_SEPARATOR . 'thumb_' . $name;

            $img = Image::make($path);

            $img->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path, 80);

            //thumbnail
            $img->fit(275)->save($path2, 80);

        }

        $product = new Product();

        $product->category_slug = $request->category_slug;
        $product->name          = $request->name;
        $product->slug          = str_slug($request->category_slug . ' ' . $product->name . ' ' . $request->weight . 'g');
        $product->image         = '/uploads/urunler' . DIRECTORY_SEPARATOR . $parent->slug . DIRECTORY_SEPARATOR . $request->category_slug . DIRECTORY_SEPARATOR . $name;
        $product->weight        = $request->weight;
        $product->description   = $request->description;
        $product->ingredients   = $request->ingredients;
        $product->active        = $request->active;


        $product->save();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $product = Product::find($id);

        if ($product === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return redirect()->route('products.index');
        }

        $categories = ProductCategory::where('parent_id', '!=', 0)->get()->pluck('name', 'slug');

        return view('admin.content.products.edit', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \HttpException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_slug' => 'required|max:255',
            'name'          => 'required|max:255',
            'image'         => 'nullable|image|mimes:jpeg,jpg,png',
            'weight'        => 'required|max:16',
            'description'   => 'required',
            'ingredients'   => 'required',
            'active'        => 'required'
        ]);

        $current_category = ProductCategory::where('slug', $request->category_slug)->first();
        $parent           = $current_category->parent_category;

        if ($request->file('image') !== null) {

            $file = $request->file('image');
            $name = str_slug($request->category_slug . ' ' . $request->name . ' ' . $request->weight . 'g') . '.' . $file->getClientOriginalExtension();

            if (!\Storage::disk('uploads')->put('urunler' . DIRECTORY_SEPARATOR . $parent->slug .
                DIRECTORY_SEPARATOR . $request->category_slug . DIRECTORY_SEPARATOR . $name, \File::get($file))) {
                throw new \HttpException(500);
            }

            $targetDir = public_path('uploads/urunler' . DIRECTORY_SEPARATOR . $parent->slug . DIRECTORY_SEPARATOR . $request->category_slug);
            $path      = $targetDir . DIRECTORY_SEPARATOR . $name;
            $path2     = $targetDir . DIRECTORY_SEPARATOR . 'thumb_' . $name;

            $img = Image::make($path);

            $img->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path, 80);

            //thumbnail
            $img->fit(275)->save($path2, 80);

        }

        $product = Product::find($id);

        $product->category_slug = $request->category_slug;
        $product->name          = $request->name;
        $product->slug          = str_slug($request->category_slug . ' ' . $product->name . ' ' . $request->weight . 'g');
        if (isset($name)) {
            $product->image = '/uploads/urunler' . DIRECTORY_SEPARATOR . $parent->slug . DIRECTORY_SEPARATOR . $request->category_slug . DIRECTORY_SEPARATOR . $name;
        }
        $product->weight      = $request->weight;
        $product->description = $request->description;
        $product->ingredients = $request->ingredients;
        $product->active      = $request->active;


        $product->save();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if ($product === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return ['status' => false];
        }

        $product->delete();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return ['status' => true];
    }

    public function get()
    {
        $data = Product::with('category')->orderBy('name', 'asc')->get();

        return ['data' => $data];
    }

    /**
     * Products sort
     * @return array
     */
    public function sort()
    {
        $i = 1;

        foreach ($_POST['item'] as &$value) {

            $faq        = Product::find($value);
            $faq->order = $i;
            $faq->save();

            $i++;
        }

        return ['status' => true];

    }
}
