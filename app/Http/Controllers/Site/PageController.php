<?php

namespace App\Http\Controllers\Site;

use App\Models\Event;
use App\Models\Faq;
use App\Models\Page;
use App\Models\PageSection;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Recipe;
use App\Models\Slide;

class PageController extends SiteBaseController
{

    public function index()
    {
        $this->isHome = true;
        $slides       = Slide::where('active', 1)->orderBy('order', 'ASC')->get();
        $sections     = PageSection::where('active', 1)->where('page_slug', 'home')->orderBy('order', 'ASC')->get();
        $cc           = ProductCategory::where('slug', 'cikolata')->with('sub_categories')->first();
        $ic           = ProductCategory::where('slug', 'il-gelato')->with('sub_categories')->first();
        $chocolates   = Product::where('active', 1)->whereIn('category_slug', $cc->sub_categories->pluck('slug'))->orderBy('order', 'ASC')->get();
        $ilgelatos    = Product::where('active', 1)->whereIn('category_slug', $ic->sub_categories->pluck('slug'))->orderBy('order', 'ASC')->get();
        $recipes      = Recipe::where('active', 1)->limit(2)->latest()->get();
        $events       = Event::where('active', 1)->limit(2)->latest()->get();

        view()->share('isHome', $this->isHome);

        return view('site.templates.home', compact('slides', 'sections', 'chocolates', 'ilgelatos', 'recipes', 'events'));
    }

    public function page($slug = null)
    {

        $page = Page::with('sections')->where('route', $slug)->where('active', 1)->first();

        if ($page === null) {
            return redirect()->route('home');
        }

        return view('site.templates.' . $page->template, compact('page'));
    }

    public function contact()
    {

        $page = Page::where('route', 'iletisim')->where('active', 1)->first();

        return view('site.templates.' . $page->template, compact('page'));
    }

    public function faqs()
    {

        $page = Page::where('route', 'sss')->where('active', 1)->first();
        $faqs = Faq::where('active', 1)->orderBy('order', 'ASC')->get();

        return view('site.templates.' . $page->template, compact('page', 'faqs'));
    }
}
