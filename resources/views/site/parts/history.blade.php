<!-- Section History  -->
<section>
    <div class="container">
        <div class="row bg-shadow border-top bg-white">
            <div class="ml-auto col-md-10 mr-auto text-center column color-brown">
                <h2>{{$sections[0]->title}}</h2>
                <p>{!! $sections[0]->content !!}</p>
                <a class="btn-primary" href="{{route('page', ['slug' =>  $sections[0]->route])}}">TARİHÇE</a>
            </div>
        </div>
    </div>
</section>