@extends('site.layouts.site')

@section('content')

    @include('site.parts.hero', ['class'=>'head-image','parent' => $page])

    <!-- Section Activity  -->
    <section id="start">
        <div class="container m-bottom-100 m-top-70">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 color-brown m-bottom-30 activit-title">
                            <h2>Pernigotti Etkinlikleri</h2>
                        </div>

                        @foreach($events as $event)
                            <div class="col-md-12">
                                <div class="activity-list">
                                    <a href="{{route('event', ['slug' => $event->slug])}}">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="activity-pic-list">
                                                    @if ($date = explode(' ', $event->when)) @endif
                                                    <h2>{{$date[0]}}</h2>
                                                    <h3>{{$date[1]}}</h3>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="activity-detail-list">
                                                    <h2>{{$event->name}}</h2>
                                                    <p></p>
                                                    <div class="activity-detail-button-home">
                                                        İNCELE <i class="ion-android-arrow-forward"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection