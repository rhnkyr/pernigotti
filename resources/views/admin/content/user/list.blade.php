@extends('admin.layouts.main')
@section('content')
    @include('admin.parts.breadcrumb',['page'=> 'Panel Kullanıcıları'])
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Panel Kullanıcı Listesi
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-lg-12 m--align-right">
                            <a href="{{route('users.create')}}"
                               class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-plus-circle"></i>
													<span>
													Yeni Ekle
													</span>
												</span>
                            </a>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet">
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="m-section__content">
                                <table class="table table-striped m-table">
                                    <thead>
                                    <tr>
                                        <th>
                                            Adı
                                        </th>
                                        <th>
                                            Kullanıcı Adı
                                        </th>
                                        <th>
                                            İşlemler
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>
                                                {{$user->name}}
                                            </td>
                                            <td>
                                                {{$user->username}}
                                            </td>
                                            <td style="width: 20%">
                                                <a href="{{route('users.edit', ['id'=>$user->id])}}"
                                                   class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                                   title="Düzenle">
                                                    <i class="la la-edit"></i>
                                                </a>

                                                <a href="#" data-id="{{$user->id}}"
                                                   class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill del"
                                                   title="Sil">
                                                    <i class="la la-trash"></i>
                                                </a>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--end::Section-->
                    </div>
                    <!--end::Form-->
                </div>

            </div>
        </div>
    </div>
@endsection

@section('page_js')

    <script>

        $(function () {
            $('body').on('click', 'a.del', function (e) {
                e.preventDefault();
                var id = $(this).data('id');

                swal({
                    title: "Uyarı!",
                    text: "Silmek istediğinize emin misiniz?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: 'btn btn-danger',
                    confirmButtonText: "Evet",
                    cancelButtonText: "İptal"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: '{{route('users.index')}}/' + id,
                            dataType: "json",
                            type: 'DELETE',
                            success: function (result) {
                                if (result.status)
                                    location.reload();
                            }
                        });
                    }
                });
            });
        });
    </script>
@endsection

