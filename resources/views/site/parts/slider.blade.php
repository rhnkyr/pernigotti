<div id="slider-home" class="royalSlider royalSlider rsMinW rsHor rsFade rsWithBullets">
    @foreach($slides as $slide)
        <div class="rsContent">
            <div class="rsBgImg" style="background-image: url('{{$slide->image}}')"></div>
            <div class="container bContainer">
                <div class="row align-items-center">
                    <div class="col-lg-6 p-r-30">
                        <div class="rsABlock">
                            <h1>{{$slide->top_text}}</h1>
                            <h2>{{$slide->middle_text}}</h2>
                        </div>
                        <div class="rsABlock">
                            <p>{!!  $slide->description !!}</p>
                        </div>
                        <div class="rsABlock" data-move-effect="left" data-fade-effect="false">
                            <a class="btn-primary btn-inline" href="{{route('page', ['slug' =>  $slide->route])}}">KEŞFET <i
                                        class="ion-ios-arrow-forward"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div><!-- /#slider-home -->