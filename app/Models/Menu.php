<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 12 Apr 2018 14:59:43 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Menu
 *
 * @property int $id
 * @property string $name
 * @property string $section
 * @property string $deletable
 *
 * @package App\Models
 */
class Menu extends Eloquent
{
    public $timestamps = false;

    protected $casts = [
        'deletable' => 'int',
    ];

    protected $fillable = [
        'name',
        'section',
        'deletable'
    ];

    public function elements()
    {
        return $this->hasMany(MenuElement::class, 'menu_section', 'section');
    }
}
