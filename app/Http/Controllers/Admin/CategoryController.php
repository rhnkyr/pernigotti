<?php

namespace App\Http\Controllers\Admin;

use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class CategoryController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.content.categories.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent_categories = ProductCategory::where('parent_id', 0)->get()->pluck('name', 'id');
        $parent_categories->prepend('ANA KATEGORİ', 0);
        return view('admin.content.categories.create', compact('parent_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \HttpException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image'  => 'nullable|image|mimes:jpeg,jpg,png',
            'name'   => 'required|max:255',
            'active' => 'required'
        ]);

        $parent = ProductCategory::find($request->parent_id);

        if ($request->file('image') !== null) {

            $file = $request->file('image');
            $name = str_slug($request->input('name')) . '.' . $file->getClientOriginalExtension();

            if (!\Storage::disk('uploads')->put('urunler' . DIRECTORY_SEPARATOR . $parent->slug . DIRECTORY_SEPARATOR . $name, \File::get($file))) {
                throw new \HttpException(500);
            }

            $targetDir = public_path('uploads/urunler' . DIRECTORY_SEPARATOR . $parent->slug);
            $path      = $targetDir . DIRECTORY_SEPARATOR . $name;

            $img = Image::make($path);

            //thumbnail
            $img->fit(getenv('IMAGE_THUMB'))->save($path, 80);

        }

        $category = new ProductCategory();

        $category->parent_id   = $request->parent_id;
        $category->name        = $request->name;
        $category->slug        = str_slug($category->name);
        $category->description = $request->description;
        $category->image       = isset($name) ? 'uploads/urunler' . DIRECTORY_SEPARATOR . $parent->slug . DIRECTORY_SEPARATOR . $name : null;
        $category->active      = $request->active;
        $category->deletable   = 1;

        $category->save();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('product-categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = ProductCategory::find($id);

        if ($category === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return redirect()->route('product-categories.index');
        }

        $parent_categories = ProductCategory::where('parent_id', 0)->get()->pluck('name', 'id');
        $parent_categories->prepend('ANA KATEGORİ', 0);

        return view('admin.content.categories.edit', compact('category', 'parent_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \HttpException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image'  => 'nullable|image|mimes:jpeg,jpg,png',
            'name'   => 'required|max:255',
            'active' => 'required'
        ]);

        $parent = ProductCategory::find($request->parent_id);

        if ($request->file('image') !== null) {

            $file = $request->file('image');
            $name = str_slug($request->input('name')) . '.' . $file->getClientOriginalExtension();

            if (!\Storage::disk('uploads')->put('urunler' . DIRECTORY_SEPARATOR . $parent->slug . DIRECTORY_SEPARATOR . $name, \File::get($file))) {
                throw new \HttpException(500);
            }

            $targetDir = public_path('uploads/urunler' . DIRECTORY_SEPARATOR . $parent->slug);
            $path      = $targetDir . DIRECTORY_SEPARATOR . 'thumb_' . $name;

            $img = Image::make($path);

            //thumbnail
            $img->fit(300)->save($path, 80);

        }

        $category = ProductCategory::find($id);

        $category->parent_id   = $request->parent_id;
        $category->name        = $request->name;
        $category->slug        = str_slug($category->name);
        $category->description = $request->description;
        $category->image       = isset($name) ? 'uploads/urunler' . DIRECTORY_SEPARATOR . $parent->slug . DIRECTORY_SEPARATOR . $name : null;
        $category->active      = $request->active;
        $category->deletable   = 1;

        $category->save();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('product-categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = ProductCategory::find($id);

        if ($event === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return ['status' => false];
        }

        $event->delete();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return ['status' => true];
    }

    public function get()
    {
        $data = ProductCategory::with('parent_category_listing')->orderBy('parent_id', 'asc')->get();

        return ['data' => $data];
    }
}
