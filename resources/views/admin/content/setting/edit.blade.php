@extends('admin.layouts.main')
@section('content')
    <!-- BEGIN: Subheader -->
    @include('admin.parts.breadcrumb',['page'=> 'Site Ayarları'])
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Bilgiler
                                </h3>
                            </div>
                        </div>
                    </div>
                    {!! Form::model($settings,['route' => ['site-settings.update', $settings->id], 'class'=>'m-form', 'method'=>'PUT']) !!}
                    <div class="m-portlet__body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group m-form__group col-lg-6">
                            {!! Form::label('site_title', 'Site Başlık :'); !!}
                            {!! Form::text('site_title', null, ['class' => 'form-control m-input']); !!}
                        </div>

                        <div class="form-group m-form__group col-lg-6">
                            {!! Form::label('facebook', 'Facebook Adresi :'); !!}
                            {!! Form::text('facebook', null, ['class' => 'form-control m-input']); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-6">
                            {!! Form::label('youtube', 'Youtube Adresi :'); !!}
                            {!! Form::text('youtube', null, ['class' => 'form-control m-input']); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-6">
                            {!! Form::label('instagram', 'Instagram Adresi :'); !!}
                            {!! Form::text('instagram', null, ['class' => 'form-control m-input']); !!}
                        </div>

                        <div class="form-group m-form__group col-lg-6">
                            {!! Form::label('privacy', 'Gizlilik Sözleşmesi :'); !!}
                            {!! Form::text('privacy', null, ['class' => 'form-control m-input']); !!}
                        </div>

                        <div class="form-group m-form__group col-lg-6">
                            {!! Form::label('legal', ' Yasal Uyarı :'); !!}
                            {!! Form::text('legal', null, ['class' => 'form-control m-input']); !!}
                        </div>

                        <div class="form-group m-form__group col-lg-12">
                            {!! Form::label('meta_description', 'Meta Description :'); !!}
                            {!! Form::text('meta_description', null, ['class' => 'form-control m-input']); !!}
                        </div>

                        <div class="form-group m-form__group col-lg-12">
                            {!! Form::label('meta_keywords', 'Meta Keywords :'); !!}
                            {!! Form::text('meta_keywords', null, ['class' => 'form-control m-input']); !!}
                        </div>

                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <button class="btn btn-primary">
                                {!! Form::submit('Kaydet', ['class'=>'btn btn-primary']) !!}
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
@endsection
