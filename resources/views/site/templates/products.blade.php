@extends('site.layouts.site')

@section('content')

    @include('site.parts.hero', ['class'=>'head-image','parent' => $page,'categories'=> $categories])

    @foreach($categories as $category)
        <section @if($loop->first)id="start"@endif>
            <div class="container-fluid no-repeat bg-chocolate-color">
                <div class="container">
                    <div class="row">
                        <div id="{{$category->slug}}" class="col-md-12 category-product">
                            <h2>{{$category->name}}</h2>
                            <p>{!! $category->desription !!}</p>
                        </div>
                    </div>
                    <div class="row">
                        @foreach($category->ordered_products as $product)

                            <div class="col-md-3 product-list chocolate-color">
                                <a href="{{route('product', ['category'=>$category->parent_category->slug, 'slug'=>$product->slug])}}">
                                    <div class="product-pic-list">
                                        <img class="img-fluid" src="{{$product->image}}"
                                             alt="{{$category->name}} {{$product->name}} {{$product->weight}}g">
                                    </div>
                                    <div class="product-detail-list">
                                        <h3>{{$product->name}}</h3>
                                        <p>{{$product->weight}} - {{$category->name}}</p>
                                    </div>
                                    <div class="product-detail-button">
                                        <span>İNCELE</span>
                                        <i class="ion-android-arrow-forward"></i>
                                    </div>
                                </a>
                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endforeach
@endsection