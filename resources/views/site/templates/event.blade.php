@extends('site.layouts.site')

@section('content')

    @include('site.parts.hero', ['class'=>'product-head-image','parent' => $page, 'child' => $event])

    <!-- Section Activity  -->
    <section>
        <div class="container m-bottom-100 m-top-70">
            <div class="row">
                <div class="col-md-12 color-brown m-bottom-30">
                    <h2>{{$event->name}}</h2>
                </div>

                @foreach($event->images as $image)

                    <div class="col-md-3 gallery">
                        <a data-toggle="modal" data-target="#exampleModalCenter-{{$loop->index}}">
                            <img src="{{$image->thumb_path}}" class="img-fluid">
                        </a>
                    </div>

                    <div class="modal fade" id="exampleModalCenter-{{$loop->index}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <img src="{{$image->path}}" class="img-fluid">
                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach

            </div>
        </div>
    </section>

@endsection