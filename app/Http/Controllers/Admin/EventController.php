<?php

namespace App\Http\Controllers\Admin;

use App\Models\Event;
use App\Models\EventImage;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class EventController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.content.event.list');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.content.event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'   => 'required|max:255',
            'when'   => 'required|max:255',
            'active' => 'required',
            'images' => 'required|array|min:1',
        ]);

        $event         = new Event();
        $event->name   = $request->name;
        $event->slug   = str_slug($event->name);
        $event->when   = $request->when;
        $event->active = $request->active;

        $event->save();

        foreach ($request->images as $img) {

            $image             = new EventImage();
            $image->event_id   = $event->id;
            $image->path       = '/uploads/etkinlik/' . $img;
            $image->thumb_path = '/uploads/etkinlik/thumb_' . $img;
            $image->active     = 1;
            $image->save();

        }

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('events.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::with('images')->find($id);

        if ($event === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return redirect()->route('events.index');
        }

        return view('admin.content.event.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'   => 'required|max:255',
            'when'   => 'required|max:255',
            'active' => 'required',
            'images' => 'required|array|min:1',
        ]);

        $event = Event::find($id);

        if ($event === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return redirect()->route('events.index');
        }

        $event->name   = $request->input('name');
        $event->slug   = str_slug($event->name);
        $event->when   = $request->input('when');
        $event->active = $request->input('active');

        $event->save();

        EventImage::where('event_id', $id)->delete();

        foreach ($request->images as $img) {

            $image             = new EventImage();
            $image->event_id   = $id;
            $image->path       = '/uploads/etkinlik/' . $img;
            $image->thumb_path = '/uploads/etkinlik/thumb_' . $img;
            $image->active     = 1;
            $image->save();

        }

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return array
     */
    public function destroy($id)
    {
        $event = Event::find($id);

        if ($event === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return redirect()->route('events.index');
        }

        $event->delete();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return ['status' => true];
    }

    public function get()
    {
        $data = Event::orderBy('created_at', 'desc')->get(['id', 'name', 'when', 'active', 'created_at']);

        return ['data' => $data];
    }

    public function imageDelete(Request $request)
    {
        $fileName = $request->url;

        try {
            $f = public_path('uploads/etkinlik' . DIRECTORY_SEPARATOR . $fileName);
            $t = public_path('uploads/etkinlik' . DIRECTORY_SEPARATOR . 'thumb_' . $fileName);

            @unlink($f);
            @unlink($t);

            echo json_encode(['status' => true, 'message' => 'Görsel silindi']);

        } catch (\Exception $ex) {
            \Log::error($ex->getMessage());
        }
    }

    public function upload()
    {
        try {
            $newName = '';
            // 5 minutes execution time
            @set_time_limit(5 * 60);

            $targetDir = public_path('uploads/etkinlik');
            // Create target dir
            if (!file_exists($targetDir) && !mkdir($targetDir) && !is_dir($targetDir)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $targetDir));
            }

            // Get a file name
            if (isset($_REQUEST['name'])) {
                $fileName = $_REQUEST['name'];
                $tmp      = explode('.', $fileName);
                $ext      = end($tmp);
            } elseif (!empty($_FILES)) {
                $fileName = $_FILES['file']['name'];
                $tmp      = explode('.', $fileName);
                $ext      = end($tmp);
            } else {
                $fileName = uniqid("file_");
            }

            if (isset($_REQUEST['event_name'])) {
                $newName = str_slug($_REQUEST['event_name']) . '-' . random_int(0, 1000) . '.' . $ext;
            }

            $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

            // Chunking might be enabled
            $chunk  = isset($_REQUEST['chunk']) ? (int)$_REQUEST['chunk'] : 0;
            $chunks = isset($_REQUEST['chunks']) ? (int)$_REQUEST['chunks'] : 0;


            if (!$out = @fopen("{$filePath}.part", $chunks ? 'ab' : 'wb')) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            }

            if (!empty($_FILES)) {
                if ($_FILES['file']['error'] || !is_uploaded_file($_FILES['file']['tmp_name'])) {
                    die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
                }

                // Read binary input stream and append it to temp file
                if (!$in = @fopen($_FILES['file']['tmp_name'], 'rb')) {
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                }
            } else {
                if (!$in = @fopen('php://input', 'rb')) {
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                }
            }

            while ($buff = fread($in, 4096)) {
                fwrite($out, $buff);
            }

            @fclose($out);
            @fclose($in);

            // Check if file has been uploaded
            if (!$chunks || $chunk == $chunks - 1) {
                // Strip the temp .part suffix off
                rename("{$filePath}.part", $filePath);
            }
            //http://api.outdoorturkiye.net/storage/app/public/images/
            \Storage::disk('uploads')->move('etkinlik/' . $fileName, 'etkinlik/' . $newName);
            $path  = $targetDir . DIRECTORY_SEPARATOR . $newName;
            $path2 = $targetDir . DIRECTORY_SEPARATOR . 'thumb_' . $newName;

            $img = Image::make($path);

            $img->resize(1200, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path, 80);

            //thumbnail
            $img->fit(300)->save($path2, 80);

            // Return Success JSON-RPC response
            die('{"jsonrpc" : "2.0", "result" : {"cleanFileName": "' . $newName . '"}, "id" : "id"}');
        } catch (\Exception $exception) {
            \Log::error('Upload error', (array)$exception->getMessage());
        }
    }
}
