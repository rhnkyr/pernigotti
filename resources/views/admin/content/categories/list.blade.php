@extends('admin.layouts.main')
@section('content')
    @include('admin.parts.breadcrumb',['page'=> 'Ürün Kategorileri'])
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Liste
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-8">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Ara..."
                                               id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                            <a href="{{route('product-categories.create')}}"
                               class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-plus-circle"></i>
													<span>
														Yeni Kategori
													</span>
												</span>
                            </a>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <div class="m_datatable" id="ajax_data"></div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
@endsection

@section('page_js')
    <script>

        $(function () {
            $('body').on('click', 'a.del', function (e) {
                e.preventDefault();
                var id = $(this).data('id');

                swal({
                    title: "Uyarı!",
                    text: "Silmek istediğinize emin misiniz?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: 'btn btn-danger',
                    confirmButtonText: "Evet",
                    cancelButtonText: "İptal"
                }).then(function(result) {
                    if (result.value) {
                        $.ajax({
                            url: '{{route('product-categories.index')}}/' + id,
                            dataType: "json",
                            type: 'DELETE',
                            success: function (result) {
                                // Do something with the result
                                if(result.status)
                                    location.reload();
                            }
                        });
                    }
                });
            });
        });

        var dtra = function () {
            //== Private functions

            var data = function () {

                var datatable = $('.m_datatable').mDatatable({
                    // datasource definition
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                // sample GET method
                                method: 'GET',
                                //url: 'https://keenthemes.com/metronic/preview/inc/api/datatables/demos/default.php',
                                url: '{{route('product-categories.get')}}',
                                map: function (raw) {
                                    // sample data mapping
                                    var dataSet = raw;
                                    if (typeof raw.data !== 'undefined') {
                                        dataSet = raw.data;
                                    }
                                    return dataSet;
                                },
                            },
                        },
                        serverPaging: false,
                        serverFiltering: false,
                        serverSorting: false,
                    },

                    translate: {
                        records: {
                            processing: 'Yükleniyor...',
                            noRecords: 'Kayıt bulunamadı',
                        },
                        toolbar: {
                            pagination: {
                                items: {
                                    default: {
                                        first: 'İlk',
                                        prev: 'Önceki',
                                        next: 'Sonraki',
                                        last: 'Son',
                                        more: 'Daha fazla',
                                        input: 'Sayfa no',
                                        select: 'Sayfalama',
                                    },
                                    @verbatim
                                    info: '{{total}} kayıttan {{start}} - {{end}} arasındaki kayıtlar gösteriliyor',
                                    @endverbatim
                                },
                            },
                        },
                    },

                    // layout definition
                    layout: {
                        scroll: false,
                        footer: false
                    },

                    // column sorting
                    sortable: false,

                    pagination: true,

                    search: {
                        input: $('#generalSearch'),
                    },

                    // columns definition
                    columns: [
                        {
                            field: 'name',
                            title: 'Kategori Adı',
                            width: 300,
                            sortable: false,
                            // basic templating support for column rendering,
                        }, {
                            field: 'parent_category_listing.name',
                            title: 'Ana Kategori',
                        }, {
                            field: 'active',
                            title: 'Durum',
                            // callback function support for column rendering
                            template: function (row) {
                                var status = {
                                    0: {'title': 'Pasif', 'class': 'm-badge--danger'},
                                    1: {'title': 'Aktif', 'class': ' m-badge--success'},
                                };
                                return '<span class="m-badge ' + status[row.active].class + ' m-badge--wide">' + status[row.active].title + '</span>';
                            },
                        }, {
                            field: 'Actions',
                            title: 'İşlemler',
                            sortable: false,
                            overflow: 'visible',
                            template: function (row, index, datatable) {
                                if(row.deletable === 0){
                                    return '\
                                    <a href="{{route('product-categories.index')}}/' + row.id + '/edit" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Düzenle">\
                                        <i class="la la-edit"></i>\
                                    </a>\
                                    ';
                                }else {
                                    return '\
                                    <a href="{{route('product-categories.index')}}/' + row.id + '/edit" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Düzenle">\
                                        <i class="la la-edit"></i>\
                                    </a>\
                                    <a href="#" data-id="' + row.id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill del" title="Sil">\
                                        <i class="la la-trash"></i>\
                                    </a>\
                                ';
                                }
                            },
                        }],
                });

                var query = datatable.getDataSourceQuery();

            };

            return {
                // public functions
                init: function () {
                    data();
                },
            };
        }();

        jQuery(document).ready(function () {
            dtra.init();
        });
    </script>
@endsection

