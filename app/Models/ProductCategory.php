<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 12 Apr 2018 14:53:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProductCategory
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $image
 * @property int $active
 * @property int $deletable
 *
 * @package App\Models
 */
class ProductCategory extends Eloquent
{
    public $timestamps = false;

    protected $casts = [
        'parent_id' => 'int',
        'deletable' => 'int',
        'active'    => 'int'
    ];

    protected $fillable = [
        'parent_id',
        'name',
        'slug',
        'description',
        'image',
        'active',
        'deletable'
    ];

    public function sub_categories()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function parent_category()
    {
        return $this->hasOne(self::class, 'id', 'parent_id');
    }

    public function parent_category_listing()
    {
        return $this->hasOne(self::class, 'id', 'parent_id')->orderBy('id', 'asc');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_slug', 'slug');
    }

    public function ordered_products()
    {
        return $this->hasMany(Product::class, 'category_slug', 'slug')->orderBy('order', 'asc');
    }
}


