<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 12 Apr 2018 13:04:53 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Recipe
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $video_id
 * @property string $ingredients
 * @property string $floor
 * @property string $decorate
 * @property string $description
 * @property int $active
 *
 * @package App\Models
 */
class Recipe extends Eloquent
{
    protected $casts = [
        'active' => 'int'
    ];

    protected $fillable = [
        'name',
        'slug',
        'video_id',
        'ingredients',
        'floor',
        'decorate',
        'description',
        'active'
    ];
}
