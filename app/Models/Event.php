<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 12 Apr 2018 13:04:53 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Event
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $when
 * @property int $active
 *
 * @package App\Models
 */
class Event extends Eloquent
{
    protected $casts = [
        'active' => 'int'
    ];

    protected $fillable = [
        'name',
        'slug',
        'when',
        'active'
    ];

    public function images()
    {
        return $this->hasMany(EventImage::class);
    }
}
