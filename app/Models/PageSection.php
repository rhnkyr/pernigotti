<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 25 Apr 2018 12:41:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PageSection
 *
 * @property int $id
 * @property string $page_slug
 * @property int $type
 * @property string $image
 * @property string $title_optional
 * @property string $title
 * @property string $route
 * @property string $content
 * @property int $image_direction
 * @property int $order
 * @property int $active
 * @property int $deletable
 *
 * @package App\Models
 */
class PageSection extends Eloquent
{
    public $timestamps = false;

    protected $casts = [
        'type'            => 'int',
        'image_direction' => 'int',
        'order'           => 'int',
        'active'          => 'int',
        'deletable'       => 'int'
    ];

    protected $fillable = [
        'page_slug',
        'type',
        'image',
        'title_optional',
        'title',
        'route',
        'content',
        'image_direction',
        'order',
        'active',
        'deletable'
    ];

    public function page()
    {
        return $this->hasOne(Page::class, 'route', 'page_slug');
    }
}
