@extends('site.layouts.site')

@section('content')

    @include('site.parts.hero', ['class'=>'product-head-image','parent' => $page, 'child' => $recipe])

    <!-- Section Activity  -->
    <section>
        <div class="container m-bottom-100 m-top-70">
            <div class="row">
                <div class="col-md-8 recipe-detail">
                    <h2>{{$recipe->name}}</h2>
                    <div class="embed-responsive embed-responsive-16by9 m-bottom-30">
                        <iframe class="embed-responsive-item"
                                src="https://www.youtube.com/embed/{{$recipe->video_id}}?rel=0"
                                allowfullscreen></iframe>
                    </div>
                </div>

                <div class="col-md-4">
                    <table class="table recipe-values">
                        <thead>
                        <tr>
                            <th colspan="2" scope="col">
                                <h3>Malzemeler</h3>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th width="48%" scope="row">Miktar</th>
                            <td width="52%">Malzemeler</td>
                        </tr>

                        @if ($arr = explode('|', $recipe->ingredients)) @endif
                        @foreach($arr as $a)
                            @if ($ings = explode(',', $a)) @endif
                            <tr>
                                <th scope="row">{{ trim($ings[0]) }}</th>
                                <td>{{ trim($ings[1]) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                @if($recipe->floor)
                <div class="col-md-4">
                    <table class="table recipe-values">
                        <thead>
                        <tr>
                            <th colspan="2" scope="col">
                                <h3>Tabanı İçin</h3>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th width="48%" scope="row">Miktar</th>
                            <td width="52%">Malzemeler</td>
                        </tr>
                        @if ($arr = explode('|', $recipe->floor)) @endif
                        @foreach($arr as $a)
                            @if ($ings = explode(',', $a)) @endif
                            <tr>
                                <th scope="row">{{ trim($ings[0]) }}</th>
                                <td>{{ trim($ings[1]) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                @endif

                <div class="col-md-4">
                    <table class="table recipe-values">
                        <thead>
                        <tr>
                            <th scope="col">
                                <h3>Süslemek İçin</h3>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($arr = explode('|', $recipe->decorate)) @endif
                        @foreach($arr as $a)
                            <tr>
                                <th scope="row">{{ trim($a) }}</th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- Section Similar -->
    <section>
        <div class="container-fluid bg-ilgelato-recipes">
            <div class="container no-padding">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 color-brown m-bottom-30">
                                <h2>Diğer Tarifler</h2>
                            </div>

                            @foreach($random_recipes as $random_recipe)

                            <div class="col-md-12">
                                <a href="{{route('recipe', ['slug'=>$random_recipe->slug])}}" class="recipe-list">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="recipe-pic-list"
                                                 style="background-image: url({{asset('assets/images/tarifler/tarif-1.jpg')}})">
                                                <img class="img-fluid" src="{{asset('assets/images/icons/play.svg')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="recipe-detail-list">
                                                <h1>{{$random_recipe->name}}</h1>
                                                <p>{{$random_recipe->description}}</p>
                                                <div class="recipe-detail-button">
                                                    İNCELE <i class="ion-android-arrow-forward"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            @endforeach

                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <a class="btn-primary" href="{{ route('recipes') }}">TÜM TARİFLER</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection