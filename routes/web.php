<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', 'Admin\GuardController@index')->name('panel.index');
Route::post('/check', 'Admin\GuardController@check')->name('login.check');

Route::group(['prefix' => 'admin-panel'], function () {
    Route::get('/dashboard', 'Admin\PanelController@index')->name('panel.welcome');

    Route::get('events/get', 'Admin\EventController@get')->name('events.get');
    Route::post('events/upload', 'Admin\EventController@upload')->name('events.upload');
    Route::post('events/delete', 'Admin\EventController@imageDelete')->name('event.image.delete');
    Route::resource('events', 'Admin\EventController');

    Route::post('faqs/sort', 'Admin\FaqController@sort')->name('faqs.sort');
    Route::resource('faqs', 'Admin\FaqController');

    Route::post('elements/sort', 'Admin\MenuElementController@sort')->name('elements.sort');
    Route::resource('menu.elements', 'Admin\MenuElementController');

    Route::resource('menus', 'Admin\MenuController');

    Route::resource('pages', 'Admin\PageController');

    Route::post('sections/sort', 'Admin\PageSectionController@sort')->name('sections.sort');
    Route::resource('page.sections', 'Admin\PageSectionController');

    Route::get('product-categories/get', 'Admin\CategoryController@get')->name('product-categories.get');
    Route::resource('product-categories', 'Admin\CategoryController');

    Route::post('products/sort', 'Admin\ProductController@sort')->name('products.sort');
    Route::get('products/get', 'Admin\ProductController@get')->name('products.get');
    Route::resource('products', 'Admin\ProductController');

    Route::get('recipes/get', 'Admin\RecipeController@get')->name('recipes.get');
    Route::resource('recipes', 'Admin\RecipeController');

    Route::resource('site-settings', 'Admin\SettingController');

    Route::post('slides/sort', 'Admin\SliderController@sort')->name('slides.sort');
    Route::resource('slides', 'Admin\SliderController');

    Route::resource('users', 'Admin\UserController');

    Route::get('/clear-cache', 'Admin\AdminBaseController@clearCache')->name('panel.clear.cache');

    Route::get('/logout', 'Admin\GuardController@logout')->name('panel.logout');
});


//Route::group(['middleware' => 'page-cache'], function () {

    Route::get('/sss', 'Site\PageController@faqs')
        ->name('sss');

    Route::get('/iletisim', 'Site\PageController@contact')
        ->name('contact');

//products
    Route::get('/etkinlikler', 'Site\EventController@events')
        ->name('events');

//product
    Route::get('/etkinlik/{slug}', 'Site\EventController@event')
        ->where('slug', '([A-Za-z0-9\-\/]+)')
        ->name('event');

//products
    Route::get('/urunler/{slug}', 'Site\ProductController@products')
        ->where('slug', '([A-Za-z0-9\-\/]+)')
        ->name('products');

//product
    Route::get('/urun/{category}/{slug}', 'Site\ProductController@product')
        ->where('category', '([A-Za-z0-9\-\/]+)')
        ->where('slug', '([A-Za-z0-9\-\/]+)')
        ->name('product');

//recipes
    Route::get('/tarifler', 'Site\RecipeController@recipes')
        ->where('slug', '([A-Za-z0-9\-\/]+)')
        ->name('recipes');

//recipe
    Route::get('/tarif/{slug}', 'Site\RecipeController@recipe')
        ->where('slug', '([A-Za-z0-9\-\/]+)')
        ->name('recipe');

//pages
    Route::get('{slug}', 'Site\PageController@page')
        ->where('slug', '([A-Za-z0-9\-\/]+)')
        ->name('page');

//home
    Route::get('/', [
        'as'   => 'home',
        'uses' => 'Site\PageController@index'
    ]);
//});