<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 12 Apr 2018 14:55:44 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Product
 *
 * @property int $id
 * @property string $category_slug
 * @property string $name
 * @property string $slug
 * @property string $image
 * @property string $weight
 * @property string $description
 * @property string $ingredients
 * @property int $active
 *
 * @package App\Models
 */
class Product extends Eloquent
{
    public $timestamps = false;

    protected $casts = [
        'active' => 'int'
    ];

    protected $fillable = [
        'category_slug',
        'name',
        'slug',
        'image',
        'weight',
        'description',
        'ingredients',
        'active'
    ];

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_slug', 'slug');
    }

    public function next($category)
    {
        return self::with('category')
            ->where('category_slug', $category)
            ->where('order', '>', $this->order)
            ->where('active', 1)
            ->orderBy('order', 'ASC')
            ->first();
    }

    public function previous($category)
    {
        return self::with('category')
            ->where('category_slug', $category)
            ->where('order', '<', $this->order)
            ->where('active', 1)
            ->orderBy('order', 'DESC')
            ->first();
    }

    public function first($category)
    {
        return self::with('category')
            ->where('category_slug', $category)
            ->where('active', 1)
            ->orderBy('order', 'ASC')
            ->first();
    }

    public function last($category)
    {
        return self::with('category')
            ->where('category_slug', $category)
            ->where('active', 1)
            ->orderBy('order', 'DESC')
            ->first();
    }
}
