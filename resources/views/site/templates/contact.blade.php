@extends('site.layouts.site')

@section('content')

    @include('site.parts.hero', ['class'=>'product-head-image','parent' => $page])

    <!-- Section Activity  -->
    <section>
        <div class="container m-bottom-100 m-top-70">
            <div class="row">
                <div class="col-md-12 color-brown m-bottom-30">
                    <h2>İletişim</h2>
                </div>

                <div class="col-md-2">
                    <div class="inner-box-maps">
                        {!! $page->page_description !!}
                    </div>
                </div>
                <div class="col-md-10">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12022.600741197233!2d29.042064268079503!3d41.120330320470906!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14caca7e62751da1%3A0x7f34e55de66664fb!2sPernigotti!5e0!3m2!1str!2str!4v1519640253251"
                            width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>

@endsection