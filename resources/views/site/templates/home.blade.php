@extends('site.layouts.site')

@section('content')
    @includeWhen($isHome,'site.parts.slider')
    @includeWhen($isHome,'site.parts.history')
    @includeWhen($isHome,'site.parts.products')
    @includeWhen($isHome,'site.parts.recipe-activity')
@endsection