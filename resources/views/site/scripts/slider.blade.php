<script>
    jQuery(document).ready(function ($) {
        $.rsCSS3Easing.easeOutBack = 'cubic-bezier(0.175, 0.885, 0.320, 1.275)';

        var slider = $('#slider-home');
        var sliderSettings = {
            arrowsNav: false,
            arrowsNavAutoHide: false,
            fadeinLoadedSlide: false,
            controlNavigationSpacing: 0,
            controlNavigation: 'none',
            imageScaleMode: 'none',
            imageAlignCenter: false,
            blockLoop: true,
            loop: true,
            numImagesToPreload: 4,
            keyboardNavEnabled: true,
            navigateByClick: false,
            transitionType: 'fade',
            block: {
                delay: 400
            },
        };
        slider.royalSlider(sliderSettings);

        // custom rsPagination
        var sliderData = slider.data('royalSlider');
        var html = '<div class="rsNav rsPagination">';
        for (var i = 0; i < sliderData.numSlides; i++) {
            txt = (i < 10) ? '0' + (i + 1) : (i + 1);
            if (i == sliderData.currSlideId) {
                html += '<div class="rsPaginationItem rsPaginationSelected" id="rsPag-' + i + '" rel="' + i + '">' + txt + '</div>';
            } else {
                html += '<div class="rsPaginationItem" id="rsPag-' + i + '" rel="' + i + '">' + txt + '</div>';
            }
        }
        html += '</div>';
        slider.append(html);

        $('.rsPaginationItem').click(function () {

            var elemRel = parseInt($(this).attr('rel'));
            if (sliderData.currSlideId === elemRel) {
                return false;
            }

            slider.royalSlider('goTo', $(this).attr('rel'));
            //sliderData.goTo($(this).attr('rel'));
            $('.rsPaginationItem').removeClass('rsPaginationSelected');
            $(this).addClass('rsPaginationSelected');
        });
        sliderData.ev.on('rsBeforeAnimStart', function (e) {
            $('.rsPaginationItem').removeClass('rsPaginationSelected');
            var currBullet = "#rsPag-" + sliderData.currSlideId;
            $(currBullet).addClass('rsPaginationSelected');
        });
        // end of custom rsPagination

    });
</script>