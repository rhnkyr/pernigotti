<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\File;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class PanelController extends AdminBaseController
{
    //
    public function index()
    {
        return view('admin.dashboard');
    }
}
