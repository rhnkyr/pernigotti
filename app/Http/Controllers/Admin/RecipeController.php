<?php

namespace App\Http\Controllers\Admin;

use App\Models\Recipe;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class RecipeController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.content.recipes.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.content.recipes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \HttpException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'              => 'required|max:255',
            'image'             => 'required|image|mimes:jpeg,jpg,png',
            'video_id'          => 'required|max:255',
            'ingredients'       => 'required|array|min:1',
            'ingredients.*.val' => 'required|string',
            'ingredients.*.inc' => 'required|string',
            //'floor'             => 'required|array|min:1',
            //'floor.*.val'       => 'required|string',
            //'floor.*.inc'       => 'required|string',
            'decorate'          => 'required|array|min:1',
            'decorate.*.inc'    => 'required|string',
            'description'       => 'required',
            'active'            => 'required'
        ]);


        if ($request->file('image') !== null) {

            $file = $request->file('image');
            $name = str_slug($request->input('name')) . '.' . $file->getClientOriginalExtension();

            if (!\Storage::disk('uploads')->put('recipes' . DIRECTORY_SEPARATOR . $name, \File::get($file))) {
                throw new \HttpException(500);
            }

            $targetDir = public_path('uploads/recipes');
            $path      = $targetDir . DIRECTORY_SEPARATOR . $name;

            $img = Image::make($path);

            $img->fit(180)->save($path, 80);

        }

        $ingredients = implode('|', array_map(function ($entry) {
            return $entry['val'] . ',' . $entry['inc'];
        }, $request->ingredients));

        if($request->floor) {
            $floor = implode('|', array_map(function ($entry) {
                return $entry['val'] . ',' . $entry['inc'];
            }, $request->floor));
        }

        $decorate = implode('|', array_map(function ($entry) {
            return $entry['inc'];
        }, $request->decorate));

        $recipe = new Recipe();

        $recipe->name        = $request->name;
        $recipe->image       = '/uploads/recipes' . DIRECTORY_SEPARATOR . $name;
        $recipe->slug        = str_slug($request->name);
        $recipe->video_id    = $request->video_id;
        $recipe->ingredients = $ingredients;
        $recipe->floor       = $floor ?? null;
        $recipe->decorate    = $decorate;
        $recipe->description = $request->description;
        $recipe->active      = $request->active;

        $recipe->save();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('recipes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recipe = Recipe::find($id);

        if ($recipe === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return redirect()->route('recipes.index');
        }


        return view('admin.content.recipes.edit', compact('recipe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \HttpException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name'              => 'required|max:255',
            'image'             => 'nullable|image|mimes:jpeg,jpg,png',
            'video_id'          => 'required|max:255',
            'ingredients'       => 'required|array|min:1',
            'ingredients.*.val' => 'required|string',
            'ingredients.*.inc' => 'required|string',
            //'floor'             => 'required|array|min:1',
            //'floor.*.val'       => 'required|string',
            //'floor.*.inc'       => 'required|string',
            'decorate'          => 'required|array|min:1',
            'decorate.*.inc'    => 'required|string',
            'description'       => 'required',
            'active'            => 'required'
        ]);

        if ($request->file('image') !== null) {

            $file = $request->file('image');
            $name = str_slug($request->input('name')) . '.' . $file->getClientOriginalExtension();

            if (!\Storage::disk('uploads')->put('recipes' . DIRECTORY_SEPARATOR . $name, \File::get($file))) {
                throw new \HttpException(500);
            }

            $targetDir = public_path('uploads/recipes');
            $path      = $targetDir . DIRECTORY_SEPARATOR . $name;

            $img = Image::make($path);

            $img->fit(180)->save($path, 80);

        }

        $ingredients = implode('|', array_map(function ($entry) {
            return $entry['val'] . ',' . $entry['inc'];
        }, $request->ingredients));

        if($request->floor[0]['val'] !== null) {
            $floor = implode('|', array_map(function ($entry) {
                return $entry['val'] . ',' . $entry['inc'];
            }, $request->floor));
        }

        $decorate = implode('|', array_map(function ($entry) {
            return $entry['inc'];
        }, $request->decorate));


        $recipe = Recipe::find($id);

        $recipe->name        = $request->name;
        if (isset($name)) {
            $recipe->image = '/uploads/recipes' . DIRECTORY_SEPARATOR . $name;
        }
        $recipe->slug        = str_slug($request->name);
        $recipe->video_id    = $request->video_id;
        $recipe->ingredients = $ingredients;
        $recipe->floor       = $floor ?? null;
        $recipe->decorate    = $decorate;
        $recipe->description = $request->description;
        $recipe->active      = $request->active;

        $recipe->save();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return redirect()->route('recipes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $recipe = Recipe::find($id);

        if ($recipe === null) {
            \Session::flash('error', 'Kayıt bulunamadı');
            return ['status' => false];
        }

        $recipe->delete();

        \Session::flash('success', 'İşleminiz gerçekleştirildi');

        return ['status' => true];
    }

    public function get()
    {
        $data = Recipe::orderBy('created_at', 'asc')->get(['id', 'name', 'active']);

        return ['data' => $data];
    }
}
