@extends('admin.layouts.main')
@section('content')
    <!-- BEGIN: Subheader -->
    @include('admin.parts.breadcrumb',['page'=> 'Tarifler'])
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Tarif Bilgileri
                                </h3>
                            </div>
                        </div>
                    </div>
                    {!! Form::open(['route' => 'recipes.store', 'class'=>'m-form' , 'files' => true]) !!}

                    <div class="m-portlet__body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group m-form__group col-lg-9">
                            {!! Form::label('image', 'Tarif Görsel :'); !!}
                            {!! Form::file('image'); !!}
                        </div>

                        <div class="form-group m-form__group col-lg-9">
                            {!! Form::label('name', 'Tarif Adı :'); !!}
                            {!! Form::text('name', null, ['class' => 'form-control m-input']); !!}
                        </div>

                        <div class="form-group m-form__group col-lg-4">
                            {!! Form::label('video_id', 'Video Id :'); !!}
                            {!! Form::text('video_id', null, ['class' => 'form-control m-input', 'placeholder' => 'Örn : https://www.youtube.com/watch?v=0pRKodLxas0 "v=" den sonraki kısım']); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-9">
                            {!! Form::label('description', 'Açıklama :'); !!}
                            {!! Form::textarea('description',null, ['class' => 'form-control m-input']); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-12">
                            <div id="m_repeater_1">
                                <div class="form-group  m-form__group" id="m_repeater_1">
                                    {!! Form::label('ingredients', 'Malzemeler :'); !!}
                                    <div data-repeater-list="ingredients" class="col-lg-10">
                                        <div data-repeater-item=""
                                             class="form-group m-form__group row align-items-center">
                                            <div class="col-md-3">
                                                <div class="m-form__group m-form__group--inline">
                                                    <div class="m-form__label">
                                                        <label>
                                                            Miktar:
                                                        </label>
                                                    </div>
                                                    <div class="m-form__control">
                                                        <input type="text" name="val" class="form-control m-input"
                                                               value=""
                                                               placeholder="Örn : 1 Su Bardağı">
                                                    </div>
                                                </div>
                                                <div class="d-md-none m--margin-bottom-10"></div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="m-form__group m-form__group--inline">
                                                    <div class="m-form__label">
                                                        <label class="m-label m-label--single">
                                                            İçerik:
                                                        </label>
                                                    </div>
                                                    <div class="m-form__control">
                                                        <input type="text" name="inc" class="form-control m-input"
                                                               value=""
                                                               placeholder="Örn: Süt">
                                                    </div>
                                                </div>
                                                <div class="d-md-none m--margin-bottom-10"></div>
                                            </div>
                                            <div class="col-md-4">
                                                <div data-repeater-delete=""
                                                     class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">
																<span>
																	<i class="la la-trash-o"></i>
																	<span>
																		Sil
																	</span>
																</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-form__group form-group row">
                                    <div class="col-lg-4">
                                        <div data-repeater-create=""
                                             class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">
														<span>
															<i class="la la-plus"></i>
															<span>
																Ekle
															</span>
														</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-form__seperator m-form__seperator--dashed  m-form__seperator--space m--margin-bottom-40"></div>
                        <div class="form-group m-form__group col-lg-12">
                            <div id="m_repeater_2">
                                <div class="form-group  m-form__group" id="m_repeater_2">
                                    {!! Form::label('floor', 'Taban :'); !!}
                                    <div data-repeater-list="floor" class="col-lg-10">
                                        <div data-repeater-item=""
                                             class="form-group m-form__group row align-items-center">
                                            <div class="col-md-3">
                                                <div class="m-form__group m-form__group--inline">
                                                    <div class="m-form__label">
                                                        <label>
                                                            Miktar:
                                                        </label>
                                                    </div>
                                                    <div class="m-form__control">
                                                        <input type="text" name="val" class="form-control m-input"
                                                               value=""
                                                               placeholder="Örn : 120 g.">
                                                    </div>
                                                </div>
                                                <div class="d-md-none m--margin-bottom-10"></div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="m-form__group m-form__group--inline">
                                                    <div class="m-form__label">
                                                        <label class="m-label m-label--single">
                                                            İçerik:
                                                        </label>
                                                    </div>
                                                    <div class="m-form__control">
                                                        <input type="text" name="inc" class="form-control m-input"
                                                               value=""
                                                               placeholder="Örn: Petibör Bisküvi">
                                                    </div>
                                                </div>
                                                <div class="d-md-none m--margin-bottom-10"></div>
                                            </div>
                                            <div class="col-md-4">
                                                <div data-repeater-delete=""
                                                     class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">
																<span>
																	<i class="la la-trash-o"></i>
																	<span>
																		Sil
																	</span>
																</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-form__group form-group row">
                                    <div class="col-lg-4">
                                        <div data-repeater-create=""
                                             class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">
														<span>
															<i class="la la-plus"></i>
															<span>
																Ekle
															</span>
														</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-form__seperator m-form__seperator--dashed  m-form__seperator--space m--margin-bottom-40"></div>
                        <div class="form-group m-form__group col-lg-12">
                            <div id="m_repeater_3">
                                <div class="form-group  m-form__group" id="m_repeater_3">
                                    {!! Form::label('decorate', 'Süsleme :'); !!}
                                    <div data-repeater-list="decorate" class="col-lg-10">
                                        <div data-repeater-item=""
                                             class="form-group m-form__group row align-items-center">
                                            <div class="col-md-6">
                                                <div class="m-form__group m-form__group--inline">
                                                    <div class="m-form__label">
                                                        <label class="m-label m-label--single">
                                                            İçerik:
                                                        </label>
                                                    </div>
                                                    <div class="m-form__control">
                                                        <input type="text" name="inc" class="form-control m-input"
                                                               value=""
                                                               placeholder="Örn: Karamel sos">
                                                    </div>
                                                </div>
                                                <div class="d-md-none m--margin-bottom-10"></div>
                                            </div>
                                            <div class="col-md-4">
                                                <div data-repeater-delete=""
                                                     class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">
																<span>
																	<i class="la la-trash-o"></i>
																	<span>
																		Sil
																	</span>
																</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-form__group form-group row">
                                    <div class="col-lg-4">
                                        <div data-repeater-create=""
                                             class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">
														<span>
															<i class="la la-plus"></i>
															<span>
																Ekle
															</span>
														</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group col-lg-4">
                            {!! Form::label('active', 'Durum :'); !!}
                            {!! Form::select('active', ['0' => 'Pasif', '1' => 'Aktif'],null, ['class' => 'form-control m-input']); !!}
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <button class="btn btn-primary">
                                {!! Form::submit('Kaydet', ['class'=>'btn btn-primary']) !!}
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
@endsection
@section('page_js')<!--begin::Page Resources -->
<script src="{{asset('assets/admin/assets/demo/default/custom/components/forms/widgets/form-repeater.js')}}"
        type="text/javascript"></script>
<!--end::Page Resources -->
@endsection
