@extends('admin.layouts.main')
@section('content')
    <!-- BEGIN: Subheader -->
    @include('admin.parts.breadcrumb',['page'=> 'Ürün Kategorileri'])
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Ürün Kategorisi Bilgiler
                                </h3>
                            </div>
                        </div>
                    </div>
                    {!! Form::model($category,['route' => ['product-categories.update', $category->id], 'class'=>'m-form', 'method'=>'PUT']) !!}
                    <div class="m-portlet__body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form-group m-form__group col-lg-9">
                            {!! Form::label('image', 'Görsel :'); !!}
                            @if($category->image)
                                <div class="form-group m-form__group col-lg-9" style="padding-left: 0 !important;">
                                    <img src="{{$category->image}}" alt="{{$category->name}}" style="width: 200px"/>
                                </div>
                            @endif
                            {!! Form::file('image'); !!}
                        </div>

                        <div class="form-group m-form__group col-lg-4">
                            {!! Form::label('parent_id', 'Kategori :'); !!}
                            {!! Form::select('parent_id', $parent_categories ,null, ['class' => 'form-control m-input']); !!}
                        </div>
                        <div class="form-group m-form__group col-lg-4">
                            {!! Form::label('name', 'Kategori Adı :'); !!}
                            {!! Form::text('name', null, ['class' => 'form-control m-input']); !!}
                        </div>

                        <div class="form-group m-form__group col-lg-6">
                            {!! Form::label('description', 'Açıklama :'); !!}
                            {!! Form::textarea('description',null, ['class' => 'form-control m-input']); !!}
                        </div>

                        <div class="form-group m-form__group col-lg-4">
                            {!! Form::label('active', 'Durum :'); !!}
                            {!! Form::select('active', ['0' => 'Pasif', '1' => 'Aktif'],null, ['class' => 'form-control m-input']); !!}
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <button class="btn btn-primary">
                                {!! Form::submit('Kaydet', ['class'=>'btn btn-primary']) !!}
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
@endsection
